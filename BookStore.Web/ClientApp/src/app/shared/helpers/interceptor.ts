import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, switchMap } from 'rxjs/operators';
import { AuthenticationService } from '../services/auth.service';
import { RefreshTokenView } from '../models/token-views/refreshtoken-view';
import { UserService } from '../services/user.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable()
export class Interceptor implements HttpInterceptor {
    public model: RefreshTokenView = new RefreshTokenView();
    constructor(private accService: AuthenticationService, private userService: UserService, private snackBar: MatSnackBar) { }
    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const accToken = localStorage.getItem('Bearer');
        req = this.settingHeader(req, accToken);
        return next.handle(req).pipe(
            catchError((error: HttpErrorResponse) => {
                if (error.status === 401) {
                    this.model.email = this.userService.getUserEmail();
                    this.model.role = this.userService.getUserRole();
                    this.refreshToken(this.model);
                    return this.accService.refreshTokens(this.model).pipe(switchMap(res => {
                        if (res) {
                            return next.handle(this.settingHeader(req, res.accessToken));
                        }
                    }));
                } else if (error.status === 500) {
                   this.snackBar.open('Check input data!', 'Undo', {duration: 3000});
                } else if (error.status === 403) {
                    this.snackBar.open('You don\'t have enough rights', 'Undo', {duration: 3000});
                } else if (error.status === 415) {
                    this.snackBar.open('Check your request', 'Undo', {duration: 3000});
                }
            })
        );
    }
    private settingHeader(req: HttpRequest<any>, token: string): HttpRequest<any> {
        const newreq = req.clone({
            setHeaders: { Authorization: 'Bearer ' + token }
        });
        return newreq;
    }
    private refreshToken(model: any) {
        this.accService.refreshTokens(model).subscribe(res => {
            localStorage.setItem('Bearer', res.accessToken);
            localStorage.setItem('Refresh', res.refreshToken);
        });
    }
}
