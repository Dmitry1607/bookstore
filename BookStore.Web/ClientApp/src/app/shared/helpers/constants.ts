export class Constants {
    public jwtEmail = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name';
    public jwtRole = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role';
    public googleClientId = '682760989570-j5spqncj4u4jr9rnqbphcc0v03bc1qgb.apps.googleusercontent.com';
}
