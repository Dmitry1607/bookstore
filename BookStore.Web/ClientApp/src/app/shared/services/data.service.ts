import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  public amountSource = new BehaviorSubject<number>(0);
  constructor() { }
  public changeAmount(totalamount: number) {
    this.amountSource.next(totalamount);
  }
  public getAmount(): number {
    return this.amountSource.getValue();
  }
}
