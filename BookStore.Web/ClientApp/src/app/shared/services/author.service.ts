import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthorView } from '../models/author-views/author-view';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { UpdateAuthorView } from '../models/author-views/update-authorview';
import { GetAuthorsView } from '../models/author-views/get-authors-view';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
  constructor(private http: HttpClient) { }

  public getAuthors(): Observable<GetAuthorsView> {
    return this.http.get<GetAuthorsView>(environment.apiUrl + '/api/author/getall');
  }
  public addAuthor(author: AuthorView): Observable<null> {
    return this.http.post<null>(environment.apiUrl + '/api/author/add', author);
  }
  public deleteAuthor(author: AuthorView): Observable<null> {
    return this.http.delete<null>(environment.apiUrl + '/api/author/delete/' + author.id);
  }
  public updateAuthor(model: UpdateAuthorView): Observable<null> {
    return this.http.post<null>(environment.apiUrl + '/api/author/update', model);
  }
}
