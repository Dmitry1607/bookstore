import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GetUsersView } from '../models/user-views/get-users-view';
import { environment } from 'src/environments/environment';
import { AddUserView } from '../models/user-views/add-user-view';
import { PaymentView } from '../models/paymentview';
import { GetUserPurchasesView } from '../models/user-views/get-purchases-view';
import { GetAllUsersPurchasesView } from '../models/user-views/get-all-users-purchases-view';
import { UpdateUserView } from '../models/user-views/update-user-view';
import { Constants } from '../helpers/constants';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    public constants: Constants = new Constants();
    constructor(private http: HttpClient, private router: Router) { }
    public getUsers(): Observable<GetUsersView> {
        return this.http.get<GetUsersView>(environment.apiUrl + '/api/account/getusers');
    }
    public addUser(newuser: AddUserView): Observable<null> {
        return this.http.post<null>(environment.apiUrl + '/api/account/adduser', newuser);
    }
    public updateUser(user: UpdateUserView): Observable<null> {
        return this.http.post<null>(environment.apiUrl + '/api/account/updateuser', user);
    }
    public deleteUser(id: string): Observable<null> {
        return this.http.delete<null>(environment.apiUrl + '/api/account/deleteuser/' + id);
    }
    public charge(payment: PaymentView): Observable<null> {
        return this.http.post<null>(environment.apiUrl + '/api/account/charge', payment);
    }
    public getUserPurchases(email: string): Observable<GetUserPurchasesView> {
        return this.http.get<GetUserPurchasesView>(environment.apiUrl + '/api/account/getuserpurchases/' + email);
    }
    public getAllUsersPurchases(): Observable<GetAllUsersPurchasesView> {
        return this.http.get<GetAllUsersPurchasesView>(environment.apiUrl + '/api/account/getalluserspurchases');
    }
    public getUserEmail(): string {
        const token = jwt_decode(localStorage.getItem('Bearer'));
        const email = token[this.constants.jwtEmail];
        return email;
    }
    public getUserRole(): string {
        const token = jwt_decode(localStorage.getItem('Bearer'));
        const role = token[this.constants.jwtRole];
        return role;
    }
    public logout() {
        localStorage.clear();
        this.router.navigate(['']);
      }
}
