import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AddBookView } from '../models/book-views/add-book-view';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GetBooksView } from '../models/book-views/get-books-view';
import { PaginationView } from '../models/book-views/pagination-view';
import { GetPaginatedBooksView } from '../models/book-views/get-paginated-books';
import { UpdateBookView } from '../models/book-views/update-book-view';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  constructor(private http: HttpClient) { }
  public addBook(book: AddBookView): Observable<null> {
    return this.http.post<null>(environment.apiUrl + '/api/book/add', book);
  }
  public updateBook(book: UpdateBookView): Observable<null> {
    return this.http.post<null>(environment.apiUrl + '/api/book/update', book);
  }
  public deleteBook(id: string): Observable<null> {
    return this.http.delete<null>(environment.apiUrl + '/api/book/delete/' + id);
  }
  public getBooks(): Observable<GetBooksView> {
    return this.http.get<GetBooksView>(environment.apiUrl + '/api/book/getall');
  }
  public getPaginatedBooks(model: PaginationView): Observable<GetPaginatedBooksView> {
    return this.http.post<GetPaginatedBooksView>(environment.apiUrl + '/api/book/getpaginatedbooks', model);
  }
  public uploadImage(uploadData: FormData): Observable<any> {
    return this.http.post<any>(environment.apiUrl + '/api/book/uploadImage', uploadData);
  }
  public getMaxPrice(): Observable<number> {
    return this.http.get<number>(environment.apiUrl + '/api/book/getmaxprice');
  }
}
