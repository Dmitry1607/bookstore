import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GenreView } from '../models/genre-views/genre-view';
import { environment } from 'src/environments/environment';
import { UpdateGenreView } from '../models/genre-views/update-genre-view';
import { GetGenresView } from '../models/genre-views/get-genres-view';

@Injectable({
    providedIn: 'root'
})
export class GenreService {
    constructor(private http: HttpClient) { }
    public getGenres(): Observable<GetGenresView> {
        return this.http.get<GetGenresView>(environment.apiUrl + '/api/genre/getall');
    }
    public addGenre(genre: GenreView): Observable<null> {
        return this.http.post<null>(environment.apiUrl + '/api/genre/add', genre);
    }
    public deleteGenre(genre: GenreView): Observable<null> {
        return this.http.delete<null>(environment.apiUrl + '/api/genre/delete/' + genre.id);
    }
    public updateGenre(model: UpdateGenreView): Observable<null> {
        return this.http.post<null>(environment.apiUrl + '/api/genre/update', model);
    }
}
