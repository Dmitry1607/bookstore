import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { UserViewModel } from 'src/app/shared/models/auth-views/registration-view';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginViewModel } from '../models/auth-views/login-view-model';
import { Token } from '../models/token-views/token';
import { RefreshTokenView } from '../models/token-views/refreshtoken-view';
import { RegistrationConfirmView } from '../models/auth-views/registration-confirm-view';
import { ExternalLoginView } from '../models/auth-views/external-login-view';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  public registration(user: UserViewModel): Observable<null> {
    return this.http.post<null>(environment.apiUrl + '/api/Account/Register', user);
  }
  public login(user: LoginViewModel): Observable<Token> {
    return this.http.post<Token>(environment.apiUrl + '/api/Account/Login', user);
  }
  public externalLogin(user: ExternalLoginView): Observable<Token> {
    return this.http.post<Token>(environment.apiUrl + '/api/Account/ExternalLogin', user);
  }
  public refreshTokens(model: RefreshTokenView): Observable<Token> {
    return this.http.post<Token>(environment.apiUrl + '/api/Account/RefreshTokens', model);
  }
  public confirm(confirmRegistration: RegistrationConfirmView) {
    return this.http.post<RegistrationConfirmView>(environment.apiUrl + '/api/account/confirmemail', confirmRegistration);
  }
}
