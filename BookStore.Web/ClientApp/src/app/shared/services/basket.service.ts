import { Injectable } from '@angular/core';
import { GetBooksView, BookGetBooksView } from '../models/book-views/get-books-view';
import { GetMagazinesView, MagazineGetMagazinesView } from '../models/magazine-views/get-magazines-view';
import { MatSnackBar } from '@angular/material';



@Injectable({
    providedIn: 'root'
})

export class BasketService {
    private basketbooks: GetBooksView = new GetBooksView();
    private basketmagazines: GetMagazinesView = new GetMagazinesView();
    constructor(private snackBar: MatSnackBar) { }
    public countTotalAmount(): number {
        let totalamount = 0;
        let booksamount = 0;
        let magazinesamount = 0;
        this.basketbooks.books = JSON.parse(localStorage.getItem('basketBooks'));
        this.basketmagazines.magazines = JSON.parse(localStorage.getItem('basketMagazines'));
        booksamount = this.basketbooks.books.reduce((sum, current) => sum + (current.price || 0), 0);
        magazinesamount = this.basketmagazines.magazines.reduce((sum, current) => sum + (current.price || 0), 0);
        totalamount = booksamount + magazinesamount;
        return totalamount;
    }
    public clearBasket() {
        this.basketbooks.books = [];
        localStorage.setItem('basketBooks', JSON.stringify(this.basketbooks.books));
        this.basketmagazines.magazines = [];
        localStorage.setItem('basketMagazines', JSON.stringify(this.basketmagazines.magazines));
    }
    public addBookToBasket(book: BookGetBooksView): void {
        this.basketbooks.books = JSON.parse(localStorage.getItem('basketBooks'));
        if (this.basketbooks.books == null) {
            this.basketbooks.books = [];
            this.basketbooks.books.push(book);
            localStorage.setItem('basketBooks', JSON.stringify(this.basketbooks.books));
        } else {
            this.basketbooks.books.push(book);
            localStorage.setItem('basketBooks', JSON.stringify(this.basketbooks.books));
        }
        this.snackBar.open('Book added to basket 🛒', 'Undo', { duration: 3000 });
    }
    public addMagazineToBasket(magazine: MagazineGetMagazinesView): void {
        this.basketmagazines.magazines = JSON.parse(localStorage.getItem('basketMagazines'));
        if (this.basketmagazines.magazines == null) {
            this.basketmagazines.magazines = [];
            this.basketmagazines.magazines.push(magazine);
            localStorage.setItem('basketMagazines', JSON.stringify(this.basketmagazines.magazines));
        } else {
            this.basketmagazines.magazines.push(magazine);
            localStorage.setItem('basketMagazines', JSON.stringify(this.basketmagazines.magazines));
        }
        this.snackBar.open('Magazine added to basket 🛒', 'Undo', { duration: 3000 });
    }
    public checkStorageExist() {
        if (!localStorage.getItem('basketBooks')) {
            this.basketbooks.books = [];
            localStorage.setItem('basketBooks', JSON.stringify(this.basketbooks.books));
        }
        if (!localStorage.getItem('basketMagazines')) {
            this.basketmagazines.magazines = [];
            localStorage.setItem('basketMagazines', JSON.stringify(this.basketmagazines.magazines));
        }
    }
}
