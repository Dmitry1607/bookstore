import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { AddMagazineView } from '../models/magazine-views/add-magazine-view';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GetMagazinesView } from '../models/magazine-views/get-magazines-view';
import { UpdateMagazineView } from '../models/magazine-views/update-magazine-view';

@Injectable({
  providedIn: 'root'
})
export class MagazineService {
  constructor(private http: HttpClient) { }
  public addMagazine(magazine: AddMagazineView): Observable<null> {
    return this.http.post<null>(environment.apiUrl + '/api/magazine/add', magazine);
  }
  public updateMagazine(magazine: UpdateMagazineView): Observable<null> {
    return this.http.post<null>(environment.apiUrl + '/api/magazine/update', magazine);
  }
  public getMagazines(): Observable<GetMagazinesView> {
    return this.http.get<GetMagazinesView>(environment.apiUrl + '/api/magazine/getall');
  }
  public deleteMagazine(id: string): Observable<null> {
    return this.http.delete<null>(environment.apiUrl + '/api/magazine/delete/' + id);
  }
}
