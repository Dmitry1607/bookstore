import { BookGetBooksView } from './book-views/get-books-view';
import { MagazineGetMagazinesView } from './magazine-views/get-magazines-view';

export class PaymentView {
    public cardnumber: string;
    public cardexpmonth: number;
    public cardexpyear: number;
    public cardcvc: string;
    public user: UserPaymentView = new UserPaymentView();
}
export class UserPaymentView {
    public email: string;
    public orderedBooks: BookGetBooksView[] = [];
    public orderedMagazines: MagazineGetMagazinesView[] = [];
}
