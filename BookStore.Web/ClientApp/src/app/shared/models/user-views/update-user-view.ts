export class UpdateUserView {
    public id: string;
    public userName: string;
    public role: string[];
    public emailConfirmed: boolean;
    constructor() {
        this.role = [];
    }
}
