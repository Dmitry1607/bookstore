export class AddUserView {
    public email: string;
    public password: string;
    public role: string;
    public emailConfirmed: boolean;
}
