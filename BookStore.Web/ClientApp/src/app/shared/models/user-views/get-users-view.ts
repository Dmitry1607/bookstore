export class GetUsersView {
    public users: UserGetUsersView[];
    constructor() {
        this.users = [];
    }
}
export class UserGetUsersView {
    public id: string;
    public userName: string;
    public email: string;
    public role: string;
    public emailConfirmed: boolean;
}
