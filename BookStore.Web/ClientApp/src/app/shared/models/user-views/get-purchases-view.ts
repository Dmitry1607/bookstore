export class GetUserPurchasesView {
    public userId: string;
    public purchases: OrderGetUserPurchasesViewItem[];
    constructor() {
        this.purchases = [];
    }
}
export class OrderGetUserPurchasesViewItem {
    public orderId: string;
    public creationDate: string;
    public books: BookGetUserPurchasesViewItem[];
    public magazines: MagazineGetUserPurchasesViewItem[];
    constructor() {
        this.books = [];
        this.magazines = [];
    }
}
export class BookGetUserPurchasesViewItem {
    public id: string;
    public title: string;
    public price: number;
    public publishDate: Date;
    public imageUrl: string;
    public description: string;
    public authors: AuthorGetUserBooksViewItem[] = [];
    public genres: GenreGetUserBooksViewItem[] = [];
}
export class AuthorGetUserBooksViewItem {
    public id: string;
    public name: string;
}
export class GenreGetUserBooksViewItem {
    public id: string;
    name: string;
}
export class MagazineGetUserPurchasesViewItem {
    public id: string;
    public title: string;
    public price: number;
    public books: BookGetUserMagazinesViewItem[] = [];
}
export class BookGetUserMagazinesViewItem {
    public id: string;
    public title: string;
    public price: number;
}
