export class GetAllUsersPurchasesView {
    public usersPurchases: UserGetAllUsersPurchasesViewItem[];
    constructor() {
        this.usersPurchases = [];
    }
}
export class UserGetAllUsersPurchasesViewItem {
    public userId: string;
    public email: string;
    public purchases: OrderGetAllUsersPurchasesViewItem[];
    constructor() {
        this.purchases = [];
    }
}


export class OrderGetAllUsersPurchasesViewItem {
    public orderId: string;
    public creationDate: string;
    public books: BookGetAllUsersPurchasesViewItem[];
    public magazines: MagazineGetAllUsersPurchasesViewItem[];
    constructor() {
        this.books = [];
        this.magazines = [];
    }
}
export class BookGetAllUsersPurchasesViewItem {
    public id: string;
    public title: string;
    public price: number;
    public publishDate: Date;
    public imageUrl: string;
    public description: string;
    public authors: AuthorGetAllUsersBooksViewItem[];
    public genres: GenreGetAllUsersBooksViewItem[];
    constructor() {
        this.authors = [];
        this.genres = [];
    }
}
export class AuthorGetAllUsersBooksViewItem {
    public id: string;
    public name: string;
}
export class GenreGetAllUsersBooksViewItem {
    public id: string;
    public name: string;
}
export class MagazineGetAllUsersPurchasesViewItem {
    public id: string;
    public title: string;
    public price: number;
    public books: BookGetAllUsersMagazinesViewItem[];
    constructor() {
        this.books = [];
    }
}
export class BookGetAllUsersMagazinesViewItem {
    public id: string;
    public title: string;
    public price: number;
}
