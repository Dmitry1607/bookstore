export class GetAuthorsView {
    public authors: GetAuthorViewItem[];
    constructor() {
        this.authors = [];
    }
}
export class GetAuthorViewItem {
    public id: string;
    public name: string;
}
