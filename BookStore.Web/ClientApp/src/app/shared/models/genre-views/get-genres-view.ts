export class GetGenresView {
    public genres: GetGenreViewItem[];
    constructor() {
        this.genres = [];
    }
}
export class GetGenreViewItem {
    public id: string;
    public name: string;
}
