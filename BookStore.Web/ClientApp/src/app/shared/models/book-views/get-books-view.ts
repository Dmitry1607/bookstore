export class GetBooksView {
    public books: BookGetBooksView[];
    constructor() {
        this.books = [];
    }
}
export class BookGetBooksView {
    public id: number;
    public title: string;
    public price: number;
    public publishDate: Date;
    public imageUrl: string;
    public description: string;
    public authors: AuthorGetBooksViewItem[];
    public genres: GenreGetBooksViewItem[];
    constructor() {
        this.authors = [];
        this.genres = [];
    }
}
export class AuthorGetBooksViewItem {
    public id: string;
    public name: string;
}
export class GenreGetBooksViewItem {
    public id: string;
    public name: string;
}
