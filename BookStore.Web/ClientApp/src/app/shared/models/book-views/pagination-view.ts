export class PaginationView {
    public currentPage: number;
    public pageSize: number;
    public minPrice: number;
    public maxPrice: number;
    public fromDate: Date;
    public toDate: Date;
    public authorsId: string[];
    public genresId: string[];
    constructor() {
        this.authorsId = [];
        this.genresId = [];
    }
}
