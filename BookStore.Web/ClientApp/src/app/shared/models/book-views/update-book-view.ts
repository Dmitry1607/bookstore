export class UpdateBookView {
    public id: number;
    public title: string;
    public price: number;
    public publishDate: Date;
    public imageUrl: string;
    public description: string;
    public authorBookList: string[];
    public bookGenreList: string[];
    constructor() {
        this.authorBookList = [];
        this.bookGenreList = [];
    }
}
