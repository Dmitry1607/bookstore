export class EditBookView {
    public id: number;
    public title: string;
    public price: number;
    public publishDate: Date;
    public imageUrl: string;
    public description: string;
    public authors: AuthorEditBooksViewItem[];
    public genres: GenreEditBooksViewItem[];
    public authorBookList: string[];
    public bookGenreList: string[];
    constructor() {
        this.authors = [];
        this.genres = [];
        this.authorBookList = [];
        this.bookGenreList = [];
    }
}
export class AuthorEditBooksViewItem {
    public id: string;
    public name: string;
}
export class GenreEditBooksViewItem {
    public id: string;
    public name: string;
}
