export class GetPaginatedBooksView {
    public totalPages: number;
    public totalItems: number;
    public pageSize: number;
    public currentPage: number;
    public books: BookGetPaginatedBooksView[];
    constructor() {
        this.books = [];
    }
}
export class BookGetPaginatedBooksView {
    public id: string;
    public title: string;
    public price: number;
    public publishDate: Date;
    public imageUrl: string;
    public description: string;
    public visible: boolean;
    public authors: AuthorGetPaginatedBooksViewItem[];
    public genres: GenreGetPaginatedBooksViewItem[];
    constructor() {
        this.authors = [];
        this.genres = [];
    }
}
export class AuthorGetPaginatedBooksViewItem {
    public id: string;
    public name: string;
}
export class GenreGetPaginatedBooksViewItem {
    public id: string;
    public name: string;
}
