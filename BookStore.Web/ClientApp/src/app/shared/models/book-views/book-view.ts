export class BookView {
    public id: number;
    public title: string;
    public price: number;
}
