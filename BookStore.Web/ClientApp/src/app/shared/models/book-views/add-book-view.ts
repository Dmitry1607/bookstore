
export class AddBookView {
    public id: string;
    public title: string;
    public price: number;
    public publishDate: Date;
    public imageUrl: string;
    public description: string;
    public authorBookList: number[];
    public bookGenreList: number[];
    constructor() {
        this.authorBookList = [];
        this.bookGenreList = [];
    }
}
