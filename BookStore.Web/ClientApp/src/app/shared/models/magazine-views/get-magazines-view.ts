export class GetMagazinesView {
    public magazines: MagazineGetMagazinesView[];
    constructor() {
        this.magazines = [];
    }
}
export class MagazineGetMagazinesView {
    public id: string;
    public title: string;
    public price: number;
    public books: BookGetMagazinesViewItem[];
    constructor() {
        this.books = [];
    }
}
export class BookGetMagazinesViewItem {
    public id: string;
    public title: string;
    public price: number;
}
