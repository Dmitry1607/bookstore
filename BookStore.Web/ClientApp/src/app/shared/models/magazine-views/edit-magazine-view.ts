export class EditMagazineView {
    public id: string;
    public title: string;
    public price: number;
    public books: BookEditMagazinesViewItem[];
    public magazineBookList: string[];
    constructor() {
        this.books = [];
        this.magazineBookList = [];
    }
}
export class BookEditMagazinesViewItem {
    public id: string;
    public title: string;
    public price: number;
}
