export class AddMagazineView {
    public title: string;
    public price: number;
    public magazineBookList: string[];
    constructor() {
        this.magazineBookList = [];
    }
}
