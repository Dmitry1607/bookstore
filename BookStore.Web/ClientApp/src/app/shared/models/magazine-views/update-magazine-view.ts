export class UpdateMagazineView {
    public id: string;
    public title: string;
    public price: number;
    public magazineBookList: string[];
    constructor() {
        this.magazineBookList = [];
    }
}
