import { Component, OnInit } from '@angular/core';
import { RegistrationConfirmView } from 'src/app/shared/models/auth-views/registration-confirm-view';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/services/auth.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-registration-confirm',
  templateUrl: './registration-confirm.component.html',
  styleUrls: ['./registration-confirm.component.css']
})
export class RegistrationConfirmComponent implements OnInit {
  public confirmRegistration: RegistrationConfirmView = new RegistrationConfirmView();
  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private authService: AuthenticationService,
              private snackBar: MatSnackBar) {
    this.confirmRegistration.email = this.activatedRoute.snapshot.queryParams.email;
    this.confirmRegistration.code = this.activatedRoute.snapshot.queryParams.token;
  }
  ngOnInit() {
  }
  public confirm() {
    this.authService.confirm(this.confirmRegistration).subscribe();
    this.snackBar.open('Email confirmed', 'Undo', {duration: 3000});
    this.router.navigate(['login']);
  }

}
