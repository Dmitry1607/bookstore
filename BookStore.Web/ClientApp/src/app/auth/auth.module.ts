import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistrationConfirmComponent } from './registration-confirm/registration-confirm.component';
import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider} from 'angular-6-social-login';
import { Constants } from '../shared/helpers/constants';


export function getAuthServiceConfigs() {
  const constans: Constants = new Constants();
  const config = new AuthServiceConfig(
      [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider(constans.googleClientId)
        }
      ]);
  return config;
}



@NgModule({
  declarations: [
    RegistrationComponent,
    LoginComponent,
    RegistrationConfirmComponent
  ],
  imports: [
    CommonModule,
    SocialLoginModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: RegistrationComponent },
    { path: 'login', component: LoginComponent },
    { path: 'confirmemail', component: RegistrationConfirmComponent}])
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ]
})
export class AuthModule { }
