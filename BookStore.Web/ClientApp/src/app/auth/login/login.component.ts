import { Component, OnInit } from '@angular/core';
import { LoginViewModel } from 'src/app/shared/models/auth-views/login-view-model';
import { AuthenticationService } from 'src/app/shared/services/auth.service';
import { Token } from '../../shared/models/token-views/token';
import * as jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';
import { AuthService, GoogleLoginProvider } from 'angular-6-social-login';
import { ExternalLoginView } from 'src/app/shared/models/auth-views/external-login-view';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user: LoginViewModel = new LoginViewModel();
  public token: Token = new Token();
  public externalUser: ExternalLoginView = new ExternalLoginView();
  constructor(private authService: AuthenticationService,
              private router: Router,
              private socialAuthService: AuthService,
              private userService: UserService) { }

  ngOnInit() {
  }
  public login(user: LoginViewModel): void {
    this.authService.login(user).subscribe((res: Token) => {
      this.token.accessToken = res.accessToken; this.token.refreshToken = res.refreshToken;
      localStorage.setItem('Bearer', this.token.accessToken);
      localStorage.setItem('Refresh', this.token.refreshToken);
      localStorage.setItem('currentUser', this.user.email);
      const test = jwt_decode(res.accessToken);
      console.log(test);
      localStorage.setItem('userRole', this.userService.getUserRole());
      if (this.token.accessToken != null && this.token.refreshToken != null) {
        this.router.navigate(['mainpage']);
      }
    });
  }
  public externalLogin() {
    const socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        this.externalUser.email = userData.email;
        this.authService.externalLogin(this.externalUser).subscribe((res: Token) => {
          this.token.accessToken = res.accessToken; this.token.refreshToken = res.refreshToken;
          localStorage.setItem('Bearer', this.token.accessToken);
          localStorage.setItem('Refresh', this.token.refreshToken);
          localStorage.setItem('currentUser', this.user.email);
          const test = jwt_decode(res.accessToken);
          console.log(test);
          localStorage.setItem('userRole', this.userService.getUserRole());
          if (this.token.accessToken != null && this.token.refreshToken != null) {
            this.router.navigate(['mainpage']);
          }
        });
        console.log('google' + ' sign in data : ', userData);
      }
    );
  }

}
