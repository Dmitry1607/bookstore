import { Component, OnInit } from '@angular/core';
import { UserViewModel } from 'src/app/shared/models/auth-views/registration-view';
import { AuthenticationService } from 'src/app/shared/services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';



@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  public angForm: FormGroup;
  public user: UserViewModel = new UserViewModel();
  constructor(private accountService: AuthenticationService, private fb: FormBuilder, private router: Router) {
    this.createForm();
  }

  ngOnInit() {
  }
  public registration(user: UserViewModel): void {
    this.accountService.registration(user).subscribe(res => {});
  }
  public createForm() {
    this.angForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required]
    });
  }
}
