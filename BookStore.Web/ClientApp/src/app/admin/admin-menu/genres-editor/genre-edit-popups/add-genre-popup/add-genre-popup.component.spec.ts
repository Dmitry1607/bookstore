import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddGenrePopupComponent } from './add-genre-popup.component';

describe('AddGenrePopupComponent', () => {
  let component: AddGenrePopupComponent;
  let fixture: ComponentFixture<AddGenrePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddGenrePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddGenrePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
