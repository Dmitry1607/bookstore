import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UpdateGenreView } from 'src/app/shared/models/genre-views/update-genre-view';
import { GenreService } from 'src/app/shared/services/genre.service';

@Component({
  selector: 'app-update-genre-popup',
  templateUrl: './update-genre-popup.component.html',
  styleUrls: ['./update-genre-popup.component.css']
})
export class UpdateGenrePopupComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<UpdateGenrePopupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: UpdateGenreView, private genreService: GenreService) { }

  ngOnInit() {
  }
  public updateGenre(model: UpdateGenreView): void {
    this.genreService.updateGenre(model).subscribe(res => { this.dialogRef.close(true); });
  }

}
