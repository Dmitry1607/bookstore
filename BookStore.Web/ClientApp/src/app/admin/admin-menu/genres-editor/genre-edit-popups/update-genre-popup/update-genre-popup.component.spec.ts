import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateGenrePopupComponent } from './update-genre-popup.component';

describe('UpdateGenrePopupComponent', () => {
  let component: UpdateGenrePopupComponent;
  let fixture: ComponentFixture<UpdateGenrePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateGenrePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGenrePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
