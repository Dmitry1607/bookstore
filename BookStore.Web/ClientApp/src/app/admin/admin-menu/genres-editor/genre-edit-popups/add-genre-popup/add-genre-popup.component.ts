import { Component, OnInit } from '@angular/core';
import { GenreView } from 'src/app/shared/models/genre-views/genre-view';
import { MatDialogRef } from '@angular/material';
import { GenreService } from 'src/app/shared/services/genre.service';

@Component({
  selector: 'app-add-genre-popup',
  templateUrl: './add-genre-popup.component.html',
  styleUrls: ['./add-genre-popup.component.css']
})
export class AddGenrePopupComponent implements OnInit {
  public model: GenreView = new GenreView();
  constructor(public dialogRef: MatDialogRef<AddGenrePopupComponent>, private genreService: GenreService) { }

  ngOnInit() {
  }
  public addAuthor(model: GenreView): void {
    this.genreService.addGenre(model).subscribe(res => { this.dialogRef.close(true); });
  }

}
