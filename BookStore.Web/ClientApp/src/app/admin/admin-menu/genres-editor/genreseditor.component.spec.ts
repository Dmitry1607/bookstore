import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenreseditorComponent } from './genreseditor.component';

describe('GenreseditorComponent', () => {
  let component: GenreseditorComponent;
  let fixture: ComponentFixture<GenreseditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenreseditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenreseditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
