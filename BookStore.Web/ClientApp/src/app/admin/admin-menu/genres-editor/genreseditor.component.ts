import { Component, OnInit } from '@angular/core';
import { GenreView } from 'src/app/shared/models/genre-views/genre-view';
import { GenreService } from 'src/app/shared/services/genre.service';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { UpdateGenrePopupComponent } from './genre-edit-popups/update-genre-popup/update-genre-popup.component';
import { AddGenrePopupComponent } from './genre-edit-popups/add-genre-popup/add-genre-popup.component';
import { GetGenresView } from 'src/app/shared/models/genre-views/get-genres-view';

@Component({
  selector: 'app-genreseditor',
  templateUrl: './genreseditor.component.html',
  styleUrls: ['./genreseditor.component.css']
})
export class GenreseditorComponent implements OnInit {
  public genres: GetGenresView = new GetGenresView();
  public dataSource = new MatTableDataSource(this.genres.genres);
  public displayedColumns: string[] = ['id', 'name', 'actions'];
  constructor(private genreService: GenreService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getGenres();
  }
  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  public openUpdateDialog(model: GenreView): void {
    const dialogUpdate = this.dialog.open(UpdateGenrePopupComponent, {
      width: '250px',
      data: { id: model.id, name: model.name }
    });
    dialogUpdate.afterClosed().subscribe(result => {
      if (result) {
        this.getGenres();
      }
    });
  }
  public openAddDialog(): void {
    const dialogAdd = this.dialog.open(AddGenrePopupComponent, { width: '250px' });
    dialogAdd.afterClosed().subscribe(result => {
      if (result) {
        this.getGenres();
      }
    });
  }
  public getGenres(): void {
    this.genreService.getGenres().subscribe(values => {
      this.genres = values;
      this.dataSource = new MatTableDataSource(this.genres.genres);
    });
  }
  public deleteGenre(genre: GenreView): void {
    this.genreService.deleteGenre(genre).subscribe(res => { this.getGenres(); });
  }
}
