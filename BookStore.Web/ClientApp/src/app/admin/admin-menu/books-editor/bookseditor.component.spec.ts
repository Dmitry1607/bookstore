import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookseditorComponent } from './bookseditor.component';

describe('BookseditorComponent', () => {
  let component: BookseditorComponent;
  let fixture: ComponentFixture<BookseditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookseditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookseditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
