import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBookPopupComponent } from './add-book-popup.component';

describe('AddBookPopupComponent', () => {
  let component: AddBookPopupComponent;
  let fixture: ComponentFixture<AddBookPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBookPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBookPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
