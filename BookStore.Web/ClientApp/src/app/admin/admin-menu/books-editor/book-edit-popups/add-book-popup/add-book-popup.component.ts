import { Component, OnInit } from '@angular/core';
import { AddBookView } from 'src/app/shared/models/book-views/add-book-view';
import { BookService } from 'src/app/shared/services/book.service';
import { MatDialogRef } from '@angular/material';
import { AuthorService } from 'src/app/shared/services/author.service';
import { GenreService } from 'src/app/shared/services/genre.service';
import { GetAuthorsView } from 'src/app/shared/models/author-views/get-authors-view';
import { GetGenresView } from 'src/app/shared/models/genre-views/get-genres-view';

@Component({
  selector: 'app-add-book-popup',
  templateUrl: './add-book-popup.component.html',
  styleUrls: ['./add-book-popup.component.css']
})
export class AddBookPopupComponent implements OnInit {
  public people$: GetAuthorsView = new GetAuthorsView();
  public genres$: GetGenresView = new GetGenresView();
  public model: AddBookView = new AddBookView();
  public selectedFile: File;
  constructor(public dialogRef: MatDialogRef<AddBookPopupComponent>,
              private bookService: BookService,
              private authorService: AuthorService,
              private genreService: GenreService) { }

  ngOnInit() {
    this.authorService.getAuthors().subscribe(res => { this.people$ = res; });
    this.genreService.getGenres().subscribe(res => { this.genres$ = res; });
  }
  public onFileChanged(event: { target: { files: File[]; }; }): void {
    this.selectedFile = event.target.files[0];
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name);
    this.bookService.uploadImage(uploadData).subscribe(res => {
      this.model.imageUrl = res;
    });
  }
  public addBook(book: AddBookView): void {
    this.bookService.addBook(book).subscribe(() => { this.dialogRef.close(true); });
  }
}
