import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateBookPopupComponent } from './update-book-popup.component';

describe('UpdateBookPopupComponent', () => {
  let component: UpdateBookPopupComponent;
  let fixture: ComponentFixture<UpdateBookPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateBookPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateBookPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
