import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UpdateBookView } from 'src/app/shared/models/book-views/update-book-view';
import { BookService } from 'src/app/shared/services/book.service';
import { EditBookView } from 'src/app/shared/models/book-views/edit-books-view';
import { AuthorService } from 'src/app/shared/services/author.service';
import { GenreService } from 'src/app/shared/services/genre.service';
import { GetAuthorsView } from 'src/app/shared/models/author-views/get-authors-view';
import { GetGenresView } from 'src/app/shared/models/genre-views/get-genres-view';

@Component({
  selector: 'app-update-book-popup',
  templateUrl: './update-book-popup.component.html',
  styleUrls: ['./update-book-popup.component.css']
})
export class UpdateBookPopupComponent implements OnInit {
  public people: GetAuthorsView = new GetAuthorsView();
  public types: GetGenresView = new GetGenresView();
  public model: UpdateBookView = new UpdateBookView();
  public selectedFile: File;
  constructor(public dialogRef: MatDialogRef<UpdateBookPopupComponent>,
              @Inject(MAT_DIALOG_DATA) public book: EditBookView,
              private bookService: BookService,
              private authorService: AuthorService,
              private genreService: GenreService) { }

  ngOnInit() {
    this.authorService.getAuthors().subscribe(res => { this.people = res; });
    this.genreService.getGenres().subscribe(res => { this.types = res; });
  }
  public onFileChanged(event: { target: { files: File[]; }; }): void {
    this.selectedFile = event.target.files[0];
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name);
    this.bookService.uploadImage(uploadData).subscribe(res => {
      this.book.imageUrl = res;
    });
  }
  public updateBook(): void {
    this.model.id = this.book.id;
    this.model.title = this.book.title;
    this.model.price = this.book.price;
    this.model.description = this.book.description;
    this.model.imageUrl = this.book.imageUrl;
    if (this.model.publishDate == null) {
      this.model.publishDate = this.book.publishDate;
    }
    this.model.authorBookList = this.book.authors.map(item => (item.id));
    this.model.bookGenreList = this.book.genres.map(item => (item.id));
    this.bookService.updateBook(this.model).subscribe(() => { this.dialogRef.close(true); });
  }
}
