import { Component, OnInit } from '@angular/core';
import { GetBooksView, BookGetBooksView } from 'src/app/shared/models/book-views/get-books-view';
import { BookService } from 'src/app/shared/services/book.service';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { UpdateBookPopupComponent } from './book-edit-popups/update-book-popup/update-book-popup.component';
import { AddBookPopupComponent } from './book-edit-popups/add-book-popup/add-book-popup.component';

@Component({
  selector: 'app-bookseditor',
  templateUrl: './bookseditor.component.html',
  styleUrls: ['./bookseditor.component.css']
})
export class BookseditorComponent implements OnInit {
  public books: GetBooksView = new GetBooksView();
  public dataSource = new MatTableDataSource(this.books.books);
  public displayedColumns: string[] = ['title', 'price', 'date', 'authors', 'genres', 'actions'];
  public bsConfig: Partial<BsDatepickerConfig>;
  constructor(private bookService: BookService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.bsConfig = Object.assign({}, { containerClass: 'theme-blue', isAnimated: true });
    this.getBooks();
  }
  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  public openUpdateDialog(model: BookGetBooksView): void {
    const dialogUpdate = this.dialog.open(UpdateBookPopupComponent,
      {
        width: '600px',
        height: '800px',
        data: {
          id: model.id,
          title: model.title,
          price: model.price,
          publishDate: model.publishDate,
          imageUrl: model.imageUrl,
          description: model.description,
          authors: model.authors,
          genres: model.genres
        }
      });
    dialogUpdate.afterClosed().subscribe(result => {
      if (result) {
        this.getBooks();
      }
    });
  }
  public openAddDialog(): void {
    const dialogRef = this.dialog.open(AddBookPopupComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getBooks();
      }
    });
  }
  public getBooks(): void {
    this.bookService.getBooks().subscribe(res => {
      this.books.books = res.books;
      this.dataSource = new MatTableDataSource(this.books.books);
    });
  }
  public deleteBook(id: string): void {
    this.bookService.deleteBook(id).subscribe(() => { this.getBooks(); });
  }

}
