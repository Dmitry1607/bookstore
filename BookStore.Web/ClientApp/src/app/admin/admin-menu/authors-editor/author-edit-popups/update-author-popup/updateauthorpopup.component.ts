import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UpdateAuthorView } from 'src/app/shared/models/author-views/update-authorview';
import { AuthorService } from 'src/app/shared/services/author.service';

@Component({
  selector: 'app-updateauthorpopup',
  templateUrl: './updateauthorpopup.component.html',
  styleUrls: ['./updateauthorpopup.component.css']
})
export class UpdateAuthorPopupComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<UpdateAuthorPopupComponent>,
              @Inject(MAT_DIALOG_DATA) public data: UpdateAuthorView, private authorService: AuthorService) { }

  ngOnInit() {
  }
  public updateAuthor(model: UpdateAuthorView): void {
    this.authorService.updateAuthor(model).subscribe(res => { this.dialogRef.close(true); });
  }
}
