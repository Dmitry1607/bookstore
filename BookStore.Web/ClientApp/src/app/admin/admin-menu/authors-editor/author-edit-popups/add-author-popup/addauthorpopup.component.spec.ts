import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAuthorPopupComponent } from './addauthorpopup.component';

describe('AddauthorpopupComponent', () => {
  let component: AddAuthorPopupComponent;
  let fixture: ComponentFixture<AddAuthorPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAuthorPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAuthorPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
