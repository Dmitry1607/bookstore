import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAuthorPopupComponent } from './updateauthorpopup.component';

describe('UpdateauthorpopupComponent', () => {
  let component: UpdateAuthorPopupComponent;
  let fixture: ComponentFixture<UpdateAuthorPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAuthorPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAuthorPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
