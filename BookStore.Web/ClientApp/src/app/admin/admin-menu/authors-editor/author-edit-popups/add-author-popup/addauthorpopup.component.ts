import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthorService } from 'src/app/shared/services/author.service';
import { AuthorView } from 'src/app/shared/models/author-views/author-view';

@Component({
  selector: 'app-addauthorpopup',
  templateUrl: './addauthorpopup.component.html',
  styleUrls: ['./addauthorpopup.component.css']
})
export class AddAuthorPopupComponent implements OnInit {
  public model: AuthorView = new AuthorView();
  constructor(public dialogRef: MatDialogRef<AddAuthorPopupComponent>, private authorService: AuthorService) { }

  ngOnInit() {
  }
  public addAuthor(model: AuthorView): void {
    this.authorService.addAuthor(model).subscribe(res => { this.dialogRef.close(true); });
  }
}
