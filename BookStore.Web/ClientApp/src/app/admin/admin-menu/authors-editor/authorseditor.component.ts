import { Component, OnInit } from '@angular/core';
import { AuthorView } from 'src/app/shared/models/author-views/author-view';
import { AuthorService } from 'src/app/shared/services/author.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material';
import { UpdateAuthorPopupComponent } from './author-edit-popups/update-author-popup/updateauthorpopup.component';
import { AddAuthorPopupComponent } from './author-edit-popups/add-author-popup/addauthorpopup.component';
import { GetAuthorsView } from 'src/app/shared/models/author-views/get-authors-view';

@Component({
  selector: 'app-authorseditor',
  templateUrl: './authorseditor.component.html',
  styleUrls: ['./authorseditor.component.css']
})
export class AuthorsEditorComponent implements OnInit {
  public authors: GetAuthorsView = new GetAuthorsView();
  public dataSource = new MatTableDataSource(this.authors.authors);
  public displayedColumns: string[] = ['id', 'name', 'actions'];
  constructor(private authorService: AuthorService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getAuthors();
  }
  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  public openUpdateDialog(model: AuthorView): void {
    const dialogUpdate = this.dialog.open(UpdateAuthorPopupComponent, {
      width: '250px',
      data: { id: model.id, name: model.name }
    });
    dialogUpdate.afterClosed().subscribe(result => {
      if (result) {
        this.getAuthors();
      }
    });
  }
  public openAddDialog(): void {
    const dialogAdd = this.dialog.open(AddAuthorPopupComponent, { width: '250px' });
    dialogAdd.afterClosed().subscribe(result => {
      if (result) {
        this.getAuthors();
      }
    });
  }
  public getAuthors(): void {
    this.authorService.getAuthors().subscribe(values => {
      this.authors = values;
      this.dataSource = new MatTableDataSource(this.authors.authors);
    });
  }
  public deleteAuthor(user: AuthorView): void {
    this.authorService.deleteAuthor(user).subscribe(() => { this.getAuthors(); });
  }
}
