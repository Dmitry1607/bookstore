import { Component, OnInit } from '@angular/core';
import { GetUsersView, UserGetUsersView } from 'src/app/shared/models/user-views/get-users-view';
import { GetAllUsersPurchasesView } from 'src/app/shared/models/user-views/get-all-users-purchases-view';
import { AddUserView } from 'src/app/shared/models/user-views/add-user-view';
import { UserService } from 'src/app/shared/services/user.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { UpdateUserPopupComponent } from './user-edit-popups/update-user-popup/update-user-popup.component';
import { AddUserPopupComponent } from './user-edit-popups/add-user-popup/add-user-popup.component';

@Component({
  selector: 'app-userseditor',
  templateUrl: './userseditor.component.html',
  styleUrls: ['./userseditor.component.css']
})
export class UserseditorComponent implements OnInit {
  public users: GetUsersView = new GetUsersView();
  public allUsersPurchases: GetAllUsersPurchasesView = new GetAllUsersPurchasesView();
  public newuser: AddUserView = new AddUserView();
  public dataSource = new MatTableDataSource(this.users.users);
  public displayedColumns: string[] = ['id', 'email', 'username', 'role', 'emailConfirmed', 'actions'];
  constructor(private userService: UserService, public dialog: MatDialog) { }

  ngOnInit() {
    this.getUsers();
  }
  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  public openUpdateDialog(model: UserGetUsersView): void {
    const dialogRef = this.dialog.open(UpdateUserPopupComponent, {
      data: {
        id: model.id,
        userName: model.userName,
        role: model.role,
        emailConfirmed: model.emailConfirmed
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getUsers();
      }
    });
  }
  public openAddDialog(): void {
    const dialogRef = this.dialog.open(AddUserPopupComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getUsers();
      }
    });
  }
  public getUsers(): void {
    this.userService.getUsers().subscribe(res => {
      this.users = res;
      this.dataSource = new MatTableDataSource(this.users.users);
    });
  }
  public deleteUser(email: string) {
    this.userService.deleteUser(email).subscribe(res => { this.getUsers(); });
  }
}
