import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UpdateUserView } from 'src/app/shared/models/user-views/update-user-view';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-update-user-popup',
  templateUrl: './update-user-popup.component.html',
  styleUrls: ['./update-user-popup.component.css']
})
export class UpdateUserPopupComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<UpdateUserPopupComponent>,
              @Inject(MAT_DIALOG_DATA) public user: UpdateUserView, private userService: UserService) { }

  ngOnInit() {
  }
  public updateUser() {
    this.userService.updateUser(this.user).subscribe(response => {this.dialogRef.close(true); });
  }

}
