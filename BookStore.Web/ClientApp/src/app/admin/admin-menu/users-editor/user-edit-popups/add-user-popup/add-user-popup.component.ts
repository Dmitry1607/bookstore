import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AddUserView } from 'src/app/shared/models/user-views/add-user-view';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-add-user-popup',
  templateUrl: './add-user-popup.component.html',
  styleUrls: ['./add-user-popup.component.css']
})
export class AddUserPopupComponent implements OnInit {
  public newuser: AddUserView = new AddUserView();
  constructor(public dialogRef: MatDialogRef<AddUserPopupComponent>, private userService: UserService) { }

  ngOnInit() {
  }
  public addUser() {
    this.userService.addUser(this.newuser).subscribe(res => { this.dialogRef.close(true); });
  }


}
