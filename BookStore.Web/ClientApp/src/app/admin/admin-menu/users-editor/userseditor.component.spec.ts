import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserseditorComponent } from './userseditor.component';

describe('UserseditorComponent', () => {
  let component: UserseditorComponent;
  let fixture: ComponentFixture<UserseditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserseditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserseditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
