import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMagazinePopupComponent } from './add-magazine-popup.component';

describe('AddMagazinePopupComponent', () => {
  let component: AddMagazinePopupComponent;
  let fixture: ComponentFixture<AddMagazinePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddMagazinePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMagazinePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
