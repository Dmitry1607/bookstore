import { Component, OnInit } from '@angular/core';
import { AddMagazineView } from 'src/app/shared/models/magazine-views/add-magazine-view';
import { MatDialogRef } from '@angular/material';
import { MagazineService } from 'src/app/shared/services/magazine.service';
import { BookService } from 'src/app/shared/services/book.service';
import { GetBooksView } from 'src/app/shared/models/book-views/get-books-view';

@Component({
  selector: 'app-add-magazine-popup',
  templateUrl: './add-magazine-popup.component.html',
  styleUrls: ['./add-magazine-popup.component.css']
})
export class AddMagazinePopupComponent implements OnInit {
  public books: GetBooksView = new GetBooksView();
  public model: AddMagazineView = new AddMagazineView();
  constructor(public dialogRef: MatDialogRef<AddMagazinePopupComponent>,
              private magazineService: MagazineService,
              private bookService: BookService) { }

  ngOnInit() {
    this.getBooks();
  }
  public getBooks() {
    this.bookService.getBooks().subscribe(res => { this.books = res; });
  }
  public addMagazine(magazine: AddMagazineView) {
    this.magazineService.addMagazine(magazine).subscribe(res => { this.dialogRef.close(true); });
  }
}
