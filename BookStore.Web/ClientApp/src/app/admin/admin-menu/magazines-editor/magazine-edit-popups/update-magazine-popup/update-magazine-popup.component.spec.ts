import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMagazinePopupComponent } from './update-magazine-popup.component';

describe('UpdateMagazinePopupComponent', () => {
  let component: UpdateMagazinePopupComponent;
  let fixture: ComponentFixture<UpdateMagazinePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateMagazinePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateMagazinePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
