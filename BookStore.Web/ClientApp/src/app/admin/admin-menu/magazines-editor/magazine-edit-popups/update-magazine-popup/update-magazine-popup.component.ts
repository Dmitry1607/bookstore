import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EditMagazineView } from 'src/app/shared/models/magazine-views/edit-magazine-view';
import { MagazineService } from 'src/app/shared/services/magazine.service';
import { BookService } from 'src/app/shared/services/book.service';
import { GetBooksView } from 'src/app/shared/models/book-views/get-books-view';
import { UpdateMagazineView } from 'src/app/shared/models/magazine-views/update-magazine-view';

@Component({
  selector: 'app-update-magazine-popup',
  templateUrl: './update-magazine-popup.component.html',
  styleUrls: ['./update-magazine-popup.component.css']
})
export class UpdateMagazinePopupComponent implements OnInit {
  public books: GetBooksView = new GetBooksView();
  public model: UpdateMagazineView = new UpdateMagazineView();
  constructor(public dialogRef: MatDialogRef<UpdateMagazinePopupComponent>,
              @Inject(MAT_DIALOG_DATA) public magazine: EditMagazineView,
              private magazineService: MagazineService,
              private bookService: BookService) { }

  ngOnInit() {
    this.getBooks();
  }
  public getBooks() {
    this.bookService.getBooks().subscribe(res => { this.books = res; });
  }
  public updateMagazine() {
    this.model.id = this.magazine.id;
    this.model.title = this.magazine.title;
    this.model.price = this.magazine.price;
    for (const book of this.magazine.books) {
      this.model.magazineBookList.push(book.id);
    }
    this.model.magazineBookList = this.magazine.books.map(item => (item.id));
    this.magazineService.updateMagazine(this.model).subscribe(res => { this.dialogRef.close(true); });
  }

}
