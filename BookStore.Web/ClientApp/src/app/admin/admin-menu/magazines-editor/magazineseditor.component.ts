import { Component, OnInit } from '@angular/core';
import { GetMagazinesView, MagazineGetMagazinesView } from 'src/app/shared/models/magazine-views/get-magazines-view';
import { MagazineService } from 'src/app/shared/services/magazine.service';
import { BookService } from 'src/app/shared/services/book.service';
import { GetBooksView } from 'src/app/shared/models/book-views/get-books-view';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { UpdateMagazinePopupComponent } from './magazine-edit-popups/update-magazine-popup/update-magazine-popup.component';
import { AddMagazinePopupComponent } from './magazine-edit-popups/add-magazine-popup/add-magazine-popup.component';

@Component({
  selector: 'app-magazineseditor',
  templateUrl: './magazineseditor.component.html',
  styleUrls: ['./magazineseditor.component.css']
})
export class MagazineseditorComponent implements OnInit {
  public magazines: GetMagazinesView = new GetMagazinesView();
  public books: GetBooksView = new GetBooksView();
  public dataSource = new MatTableDataSource(this.magazines.magazines);
  public displayedColumns: string[] = ['title', 'price', 'books', 'actions'];
  constructor(private magazineService: MagazineService,
              private bookService: BookService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.getBooks();
    this.getMagazines();
  }
  public openUpdateDialog(model: MagazineGetMagazinesView): void {
    const dialogUpdate = this.dialog.open(UpdateMagazinePopupComponent,
      {
        width: '600px',
        data: {
          id: model.id,
          title: model.title,
          price: model.price,
          books: model.books
        }
      });
    dialogUpdate.afterClosed().subscribe(result => {
      if (result) {
        this.getMagazines();
      }
    });
  }
  public openAddDialog(): void {
    const dialogRef = this.dialog.open(AddMagazinePopupComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getMagazines();
      }
    });
  }
  public applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  public getBooks() {
    this.bookService.getBooks().subscribe(res => { this.books = res; });
  }
  public getMagazines() {
    this.magazineService.getMagazines().subscribe(res => {
      this.magazines.magazines = res.magazines;
      this.dataSource = new MatTableDataSource(this.magazines.magazines);
    });
  }
  public deleteMagazine(id: string) {
    this.magazineService.deleteMagazine(id).subscribe(() => { this.getMagazines(); });
  }
}
