import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MagazineseditorComponent } from './magazineseditor.component';

describe('MagazineseditorComponent', () => {
  let component: MagazineseditorComponent;
  let fixture: ComponentFixture<MagazineseditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MagazineseditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MagazineseditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
