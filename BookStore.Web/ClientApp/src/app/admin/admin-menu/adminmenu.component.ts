import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adminmenu',
  templateUrl: './adminmenu.component.html',
  styleUrls: ['./adminmenu.component.css']
})
export class AdminmenuComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  public openAuthor() {
    this.router.navigate(['adminmenu/authorseditor']);
  }
  public openBook() {
    this.router.navigate([ 'adminmenu/bookseditor']);
  }
  public openGenre() {
    this.router.navigate(['adminmenu/genreseditor']);
  }
  public openMagazine() {
    this.router.navigate([ 'adminmenu/magazineseditor' ]);
  }
  public openUser() {
    this.router.navigate(['adminmenu/userseditor' ]);
  }

}
