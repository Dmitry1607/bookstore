import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminmenuComponent } from './admin-menu/adminmenu.component';
import { GenreseditorComponent } from './admin-menu/genres-editor/genreseditor.component';
import { BookseditorComponent } from './admin-menu/books-editor/bookseditor.component';
import { MagazineseditorComponent } from './admin-menu/magazines-editor/magazineseditor.component';
import { UserseditorComponent } from './admin-menu/users-editor/userseditor.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { AddBookPopupComponent } from './admin-menu/books-editor/book-edit-popups/add-book-popup/add-book-popup.component';
import { UpdateBookPopupComponent } from './admin-menu/books-editor/book-edit-popups/update-book-popup/update-book-popup.component';
import { AddGenrePopupComponent } from './admin-menu/genres-editor/genre-edit-popups/add-genre-popup/add-genre-popup.component';
import { UpdateGenrePopupComponent } from './admin-menu/genres-editor/genre-edit-popups/update-genre-popup/update-genre-popup.component';
// tslint:disable-next-line:max-line-length
import { AddMagazinePopupComponent } from './admin-menu/magazines-editor/magazine-edit-popups/add-magazine-popup/add-magazine-popup.component';
// tslint:disable-next-line:max-line-length
import { UpdateMagazinePopupComponent } from './admin-menu/magazines-editor/magazine-edit-popups/update-magazine-popup/update-magazine-popup.component';
import { AddUserPopupComponent } from './admin-menu/users-editor/user-edit-popups/add-user-popup/add-user-popup.component';
import { UpdateUserPopupComponent } from './admin-menu/users-editor/user-edit-popups/update-user-popup/update-user-popup.component';
import { AuthGuard } from '../shared/helpers/auth-guard';
import { AuthorsEditorComponent } from './admin-menu/authors-editor/authorseditor.component';
import { AddAuthorPopupComponent } from './admin-menu/authors-editor/author-edit-popups/add-author-popup/addauthorpopup.component';
import { UpdateAuthorPopupComponent } from './admin-menu/authors-editor/author-edit-popups/update-author-popup/updateauthorpopup.component';
import { BookstoreModule } from '../bookstore/bookstore.module';




@NgModule({
  declarations: [
    AddAuthorPopupComponent,
    UpdateAuthorPopupComponent,
    AddBookPopupComponent,
    UpdateBookPopupComponent,
    AddGenrePopupComponent,
    UpdateGenrePopupComponent,
    AddMagazinePopupComponent,
    UpdateMagazinePopupComponent,
    AddUserPopupComponent,
    UpdateUserPopupComponent,
    AdminmenuComponent,
    AuthorsEditorComponent,
    GenreseditorComponent,
    BookseditorComponent,
    MagazineseditorComponent,
    UserseditorComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    NgSelectModule,
    BookstoreModule,
    BrowserAnimationsModule,
    ButtonsModule.forRoot(),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    RouterModule.forChild([{
      path: 'adminmenu', component: AdminmenuComponent, canActivate: [AuthGuard], children: [
        { path: '', redirectTo: 'authorseditor', pathMatch: 'full' },
        { path: 'authorseditor', component: AuthorsEditorComponent, canActivate: [AuthGuard] },
        { path: 'bookseditor', component: BookseditorComponent, canActivate: [AuthGuard] },
        { path: 'genreseditor', component: GenreseditorComponent, canActivate: [AuthGuard] },
        { path: 'magazineseditor', component: MagazineseditorComponent, canActivate: [AuthGuard] },
        { path: 'userseditor', component: UserseditorComponent, canActivate: [AuthGuard] }
      ]
    },
    ])
  ],
  exports: [RouterModule],
  entryComponents: [UpdateAuthorPopupComponent,
    AddAuthorPopupComponent,
    AddBookPopupComponent,
    UpdateBookPopupComponent,
    AddGenrePopupComponent,
    UpdateGenrePopupComponent,
    AddMagazinePopupComponent,
    UpdateMagazinePopupComponent,
    AddUserPopupComponent,
    UpdateUserPopupComponent]
})
export class AdminModule { }
