import { Component, OnInit } from '@angular/core';
import { MatDialog, } from '@angular/material/dialog';
import { AuthorService } from 'src/app/shared/services/author.service';
import { GetBooksView, BookGetBooksView } from 'src/app/shared/models/book-views/get-books-view';
import { Options } from 'ng5-slider';
import { BookFilterView } from 'src/app/shared/models/book-views/book-filter-view';
import { GetMagazinesView, MagazineGetMagazinesView } from 'src/app/shared/models/magazine-views/get-magazines-view';
import { PaginationView } from 'src/app/shared/models/book-views/pagination-view';
import { GetPaginatedBooksView } from 'src/app/shared/models/book-views/get-paginated-books';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { BookService } from 'src/app/shared/services/book.service';
import { GenreService } from 'src/app/shared/services/genre.service';
import { MagazineService } from 'src/app/shared/services/magazine.service';
import { BasketService } from 'src/app/shared/services/basket.service';
import { GetAuthorsView } from 'src/app/shared/models/author-views/get-authors-view';
import { GetGenresView } from 'src/app/shared/models/genre-views/get-genres-view';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {
  public m: number;
  public role: string;
  public filterAuthors: GetAuthorsView = new GetAuthorsView();
  public filterGenres: GetGenresView = new GetGenresView();
  public books: GetPaginatedBooksView = new GetPaginatedBooksView();
  public magazines: GetMagazinesView = new GetMagazinesView();
  public basketbooks: GetBooksView = new GetBooksView();
  public basketmagazines: GetMagazinesView = new GetMagazinesView();
  public searchText: string;
  public rangeValues: BookFilterView = {
    minValue: 0,
    maxValue: 1000
  };
  public options: Options = {
    floor: 0,
    ceil: 1000
  };
  public paginationView: PaginationView = {
    currentPage: 1,
    pageSize: 5,
    authorsId: [],
    genresId: [],
    minPrice: 0,
    maxPrice: 1000,
    fromDate: null,
    toDate: null
  };
  public authorsId: string[] = [];
  public genresId: string[] = [];
  public sizes: number[] = [2, 3, 5];
  public bsValue = new Date();
  public bsRangeValue: Date[] = [];
  public show = false;
  public bsConfig: Partial<BsDatepickerConfig>;

  constructor(public dialog: MatDialog,
              private authorService: AuthorService,
              private bookService: BookService,
              private genreService: GenreService,
              private magazineService: MagazineService,
              private basketService: BasketService) {
  }

  ngOnInit() {
    this.basketService.checkStorageExist();
    this.getAuthorsAndGenres();
    this.getPaginatedBooks();
    this.getMagazines();
    this.bookService.getMaxPrice().subscribe(res => {
      this.options.ceil = res;
      this.rangeValues.maxValue = this.options.ceil;
      this.paginationView.maxPrice = res;
    });
    this.bsConfig = Object.assign({}, { containerClass: 'theme-blue', isAnimated: true });
    this.paginationView.minPrice = this.rangeValues.minValue;
    this.paginationView.maxPrice = this.rangeValues.maxValue;
  }
  public getAuthorsAndGenres() {
    this.authorService.getAuthors().subscribe(res => { this.filterAuthors = res; });
    this.genreService.getGenres().subscribe(res => { this.filterGenres = res; });
  }
  public getPaginatedBooks(): void {
    this.bookService.getPaginatedBooks(this.paginationView).subscribe(res => {
      this.books = res;
    });
  }
  public pagEvent(event: any) {
    this.paginationView.currentPage = event;
    this.paginationView.minPrice = this.rangeValues.minValue;
    this.paginationView.maxPrice = this.rangeValues.maxValue;
    if (this.bsRangeValue == null) {
      this.paginationView.fromDate = null;
      this.paginationView.toDate = null;
    } else {
      this.paginationView.fromDate = this.bsRangeValue[0];
      this.paginationView.toDate = this.bsRangeValue[1];
    }
    this.getPaginatedBooks();
  }
  public pageSizeChanging() {
    this.paginationView.minPrice = this.rangeValues.minValue;
    this.paginationView.maxPrice = this.rangeValues.maxValue;
    if (this.bsRangeValue == null) {
      this.paginationView.fromDate = null;
      this.paginationView.toDate = null;
    } else {
      this.paginationView.fromDate = this.bsRangeValue[0];
      this.paginationView.toDate = this.bsRangeValue[1];
    }
    this.getPaginatedBooks();
  }
  public getFilteredBooks() {
    this.paginationView.minPrice = this.rangeValues.minValue;
    this.paginationView.maxPrice = this.rangeValues.maxValue;
    if (this.bsRangeValue == null) {
      this.paginationView.fromDate = null;
      this.paginationView.toDate = null;
    } else {
      this.paginationView.fromDate = this.bsRangeValue[0];
      this.paginationView.toDate = this.bsRangeValue[1];
    }
    this.getPaginatedBooks();
  }
  public getMagazines(): void {
    this.magazineService.getMagazines().subscribe(res => {
      this.magazines.magazines = res.magazines;
    });
  }
  public addBookToBasket(book: BookGetBooksView): void {
    this.basketService.addBookToBasket(book);
  }
  public addMagazineToBasket(magazine: MagazineGetMagazinesView): void {
    this.basketService.addMagazineToBasket(magazine);
  }
}
