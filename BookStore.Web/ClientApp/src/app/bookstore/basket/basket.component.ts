import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GetBooksView } from 'src/app/shared/models/book-views/get-books-view';
import { GetMagazinesView } from 'src/app/shared/models/magazine-views/get-magazines-view';
import { MatDialog } from '@angular/material/dialog';
import { DataService } from 'src/app/shared/services/data.service';
import { BasketService } from 'src/app/shared/services/basket.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {
  public basketbooks: GetBooksView = new GetBooksView();
  public basketmagazines: GetMagazinesView = new GetMagazinesView();
  public totalamount = 0;
  constructor(public dialog: MatDialog, private data: DataService, private basketService: BasketService) { }

  ngOnInit() {
    this.basketbooks.books = JSON.parse(localStorage.getItem('basketBooks'));
    this.basketmagazines.magazines = JSON.parse(localStorage.getItem('basketMagazines'));
    this.totalamount = this.basketService.countTotalAmount();
  }
  public pay(totalamount: number) {
    this.data.changeAmount(totalamount);
    this.dialog.closeAll();
  }
  public clearBasket() {
    this.basketService.clearBasket();
    this.totalamount = 0;
  }
}
