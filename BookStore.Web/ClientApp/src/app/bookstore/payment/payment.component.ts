import { Component, OnInit } from '@angular/core';
import { GetBooksView } from 'src/app/shared/models/book-views/get-books-view';
import { GetMagazinesView } from 'src/app/shared/models/magazine-views/get-magazines-view';
import { PaymentView } from 'src/app/shared/models/paymentview';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';
import { DataService } from 'src/app/shared/services/data.service';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  public basketbooks: GetBooksView = new GetBooksView();
  public basketmagazines: GetMagazinesView = new GetMagazinesView();
  public totalamount = 0;
  public payment: PaymentView = new PaymentView();
  constructor(private userService: UserService, private router: Router, private dataService: DataService) { }
  ngOnInit() {
    this.totalamount = this.dataService.getAmount();
  }
  public charge(payment: PaymentView) {
    payment.user.email = this.userService.getUserEmail();
    payment.user.orderedBooks = JSON.parse(localStorage.getItem('basketBooks'));
    payment.user.orderedMagazines = JSON.parse(localStorage.getItem('basketMagazines'));
    this.userService.charge(payment).subscribe(res => {
      if (res === true) {
        this.router.navigate(['successfulpayment']);
        this.basketbooks.books = [];
        this.basketmagazines.magazines = [];
        localStorage.setItem('basketBooks', JSON.stringify(this.basketbooks.books));
        localStorage.setItem('basketMagazines', JSON.stringify(this.basketmagazines.magazines));
      }
    });
  }





}
