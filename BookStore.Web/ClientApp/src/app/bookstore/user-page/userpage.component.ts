import { Component, OnInit } from '@angular/core';
import { GetUserPurchasesView } from 'src/app/shared/models/user-views/get-purchases-view';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-userpage',
  templateUrl: './userpage.component.html',
  styleUrls: ['./userpage.component.css']
})
export class UserpageComponent implements OnInit {
  public email: string;
  public userPurchases: GetUserPurchasesView = new GetUserPurchasesView();
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    const bearer = localStorage.getItem('Bearer');
    if (bearer == null) {
      this.router.navigate(['/']);
    }
    this.email = this.userService.getUserEmail();
    this.getUserPurchases(this.email);
  }
  public getUserPurchases(email: string) {
    this.userService.getUserPurchases(email).subscribe(res => {
      this.userPurchases = res;
    });
  }
}
