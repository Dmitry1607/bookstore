import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { MainpageComponent } from './main-page/mainpage.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { BasketComponent } from './basket/basket.component';
import { SearchLine } from '../shared/helpers/searchline.pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng5SliderModule } from 'ng5-slider';
import { PaymentComponent } from './payment/payment.component';
import { UserpageComponent } from './user-page/userpage.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SuccessfulpaymentComponent } from './successful-payment/successfulpayment.component';
import { AlertModule } from 'ngx-bootstrap/alert';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HeaderComponent } from './header/header.component';




@NgModule({
  declarations: [
    BasketComponent,
    SearchLine,
    MainpageComponent,
    PaymentComponent,
    SuccessfulpaymentComponent,
    UserpageComponent,
    HeaderComponent],
  imports: [
    Ng2SearchPipeModule,
    Ng5SliderModule,
    CommonModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    FormsModule,
    NgxPaginationModule,
    NgSelectModule,
    NgbModule,
    AlertModule.forRoot(),
    BsDatepickerModule.forRoot(),
    RouterModule.forChild([{ path: 'mainpage', component: MainpageComponent },
    { path: 'payment', component: PaymentComponent },
    { path: 'userpage', component: UserpageComponent }
    ])
  ],
  entryComponents: [BasketComponent],
  exports: [HeaderComponent]
})
export class BookstoreModule { }
