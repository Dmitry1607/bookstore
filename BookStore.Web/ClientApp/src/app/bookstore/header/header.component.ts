import { Component, OnInit } from '@angular/core';
import { BasketComponent } from '../basket/basket.component';
import { UserService } from 'src/app/shared/services/user.service';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public role: string;
  constructor(private userService: UserService, public dialog: MatDialog, private router: Router) { }

  ngOnInit() {
    this.role = this.userService.getUserRole();
  }
  public openAdminPage() {
    this.router.navigate(['adminmenu']);
  }
  public openBasket() {
    const dialogRef = this.dialog.open(BasketComponent);
    dialogRef.afterClosed().subscribe();
  }
  public logout() {
    this.userService.logout();
  }
}
