import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessfulpaymentComponent } from './successfulpayment.component';

describe('SuccessfulpaymentComponent', () => {
  let component: SuccessfulpaymentComponent;
  let fixture: ComponentFixture<SuccessfulpaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessfulpaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessfulpaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
