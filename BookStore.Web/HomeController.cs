﻿using Microsoft.AspNetCore.Mvc;

namespace BookStore.Web
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
