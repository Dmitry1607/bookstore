﻿namespace BookStore.BLL.Helpers
{
    public class EmailConfig
    {
        public string AppName { get; set; }
        public string Email { get; set; }
        public string EmailPassword { get; set; }
        public string Host { get; set; }
        public int Port { get; set;}
        public bool UseSsl { get; set; }

    }
}
