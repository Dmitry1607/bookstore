﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace BookStore.BLL.Helpers.HelpersInterfaces
{
    public interface IUploadImageHelper
    {
        Task<string> UploadImage(IFormFile file);
    }
}
