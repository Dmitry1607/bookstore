using System.Threading.Tasks;
using MimeKit;
using MailKit.Net.Smtp;
using BookStore.BLL.Interfaces;
using BookStore.BLL.Helpers;
using Microsoft.Extensions.Options;

namespace BookStore.BLL.Services
{
    public class EmailService : IEmailService
    {
        private readonly IOptions<EmailConfig> _options;

        public EmailService(IOptions<EmailConfig> options)
        {
            _options = options;
        }
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(_options.Value.AppName, _options.Value.Email));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync(_options.Value.Host, _options.Value.Port, _options.Value.UseSsl);
                await client.AuthenticateAsync(_options.Value.Email, _options.Value.EmailPassword);
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
        }
    }
}
