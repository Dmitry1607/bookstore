﻿using BookStore.BLL.Helpers.HelpersInterfaces;
using Dropbox.Api;
using Dropbox.Api.Files;
using Dropbox.Api.Sharing;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.BLL.Helpers
{
    public class UploadImageHelper : IUploadImageHelper
    {
        private readonly IConfiguration _configuration;

        public UploadImageHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<string> UploadImage(IFormFile file)
        {
            using (var dbx = new DropboxClient(_configuration["DropBoxToken"]))
            {
                using (var image = Image.FromStream(file.OpenReadStream()))
                {
                    if (!ImageFormat.Jpeg.Equals(image.RawFormat) && !ImageFormat.Png.Equals(image.RawFormat))
                    {
                        throw new ArgumentException("Inappropriate format!");
                    }
                    if (image.Width != 140 && image.Height != 200)
                    {
                        throw new ArgumentException("Inappropriate resolution!");
                    }
                }
                string folder = _configuration["DropBoxDestinationFolder"];
                using (var mem = new MemoryStream())
                {
                    FileMetadata updated = await dbx.Files.UploadAsync(folder + "/" + file.FileName, WriteMode.Overwrite.Instance, body: file.OpenReadStream());
                    ListSharedLinksResult exist = await dbx.Sharing.ListSharedLinksAsync(folder + "/" + file.FileName);
                    if (exist.Links.Count == 0)
                    {
                        Task<SharedLinkMetadata> tx = dbx.Sharing.CreateSharedLinkWithSettingsAsync(folder + "/" + file.FileName);
                        tx.Wait();
                        string url = tx.Result.Url;
                        url = url.Remove(0, 24);
                        return _configuration["DropBoxImage"] + url;
                    }
                    else
                    {
                        string url = exist.Links.First().Url.Replace(_configuration["DropBoxUrl"], _configuration["DropBoxImage"]);
                        return url;
                    }
                }
            }
        }
    }
}
