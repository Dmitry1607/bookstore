﻿using BookStore.BLL.Interfaces;
using BookStore.BLL.Views;
using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BookStore.BLL.Views.BookViews;
using BookStore.DAL.Models;
using BookStore.BLL.Helpers.HelpersInterfaces;

namespace BookStore.BLL.Services
{
    public class BookService : IBookService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAuthorBookRepository _authorBookRepository;
        private readonly IBookGenreRepository _bookGenreRepository;
        private readonly IBookRepository _bookRepository;
        private readonly IConfiguration _configuration;
        private readonly IUploadImageHelper _uploadImageHelper;

        public BookService(IUnitOfWork uow, 
            IAuthorBookRepository repoab, 
            IBookGenreRepository bookGenreRepository, 
            IBookRepository bookRepository, 
            IConfiguration configuration,
            IUploadImageHelper uploadImageHelper)
        {
            _unitOfWork = uow;
            _authorBookRepository = repoab;
            _bookGenreRepository = bookGenreRepository;
            _bookRepository = bookRepository;
            _configuration = configuration;
            _uploadImageHelper = uploadImageHelper;
        }
        public async Task<string> UploadImage(IFormFile file)
        {
            return await _uploadImageHelper.UploadImage(file);
        }
        public async Task Add(AddBookView bookView)
        {
            var book = new Book
            {
                Title = bookView.Title,
                Price = bookView.Price,
                PublishDate = bookView.PublishDate.ToString("MM/dd/yyyy"),
                Description = bookView.Description,
                ImageUrl = bookView.ImageUrl
            };
            await _unitOfWork.Books.Create(book);
            var mylist1 = bookView.AuthorBookList.Select(authorId => new AuthorBook()
            {
                BookId = book.Id,
                AuthorId = authorId
            }).ToList();
            var mylist2 = bookView.BookGenreList.Select(genreId => new BookGenre()
            {
                BookId = book.Id,
                GenreId = genreId
            }).ToList();
            await _authorBookRepository.AddRange(mylist1);
            await _bookGenreRepository.AddRange(mylist2);
        }
        public async Task Update(UpdateBookView bookView)
        {
            Book book = await _unitOfWork.Books.Get(bookView.Id);
            if (book == null)
            {
                throw new ArgumentException("Book not found");
            }
            book.Title = bookView.Title;
            book.Price = bookView.Price;
            book.PublishDate = bookView.PublishDate.ToString("MM/dd/yyyy");
            book.ImageUrl = bookView.ImageUrl;
            book.Description = bookView.Description;
            await _unitOfWork.Books.Update(book);
            await _authorBookRepository.DeleteByBookId(book.Id);
            await _bookGenreRepository.DeleteByBookId(book.Id);
            var authorBookList = bookView.AuthorBookList.Select(authorId => new AuthorBook()
            {
                BookId = book.Id,
                AuthorId = authorId
            }).ToList();
            var bookGenreList = bookView.BookGenreList.Select(genreId => new BookGenre()
            {
                BookId = book.Id,
                GenreId = genreId
            }).ToList();
            await _authorBookRepository.AddRange(authorBookList);
            await _bookGenreRepository.AddRange(bookGenreList);
        }
        public async Task<GetBookView> Get(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ValidationException("Id is incorrect");
            }
            Book book = await _unitOfWork.Books.Get(id);
            if (book == null)
            {
                throw new ValidationException("Book is not found");
            }
            book.AuthorBooks = (await _authorBookRepository.GetByBookId(book.Id)).ToList();
            book.BookGenres = (await _bookGenreRepository.GetByBookId(book.Id)).ToList();
            var result = new GetBookView
            {
                Id = book.Id,
                Title = book.Title,
                Price = book.Price,
                Description = book.Description,
                ImageUrl = book.ImageUrl,
                PublishDate = DateTime.Parse(book.PublishDate),
                Authors = book.AuthorBooks.Where(authorBook => authorBook.Book.Id == book.Id).Select(authorBook => new AuthorGetBookViewItem()
                {
                    Id = authorBook.Author.Id,
                    Name = authorBook.Author.Name
                }).ToList(),
                Genres = book.BookGenres.Where(bookGenre => bookGenre.Book.Id == book.Id).Select(bookGenre => new GenreGetBookViewItem()
                {
                    Id = bookGenre.Genre.Id,
                    Name = bookGenre.Genre.Name
                }).ToList()
            };
            return result;
        }
        public async Task<GetBooksView> GetAll()
        {
            IEnumerable<Book> books = await _bookRepository.Get();
            IEnumerable<AuthorBook> authorBooks = await _authorBookRepository.GetIncludedElements();
            IEnumerable<BookGenre> bookGenres = await _bookGenreRepository.GetIncludedlements();
            var result = new GetBooksView
            {
                Books = books.Select(book => MapBook(book, authorBooks.Where(authorBook => authorBook.Book.Id == book.Id).ToList(),
                bookGenres.Where(bookGenre => bookGenre.Book.Id == book.Id).ToList())).ToList()
            };
            return result;
        }
        public async Task<GetPaginatedBooksView> GetPaginatedBooks(BooksPaginationView paginationView)
        {
            if (paginationView.FromDate.ToString() == "1/1/0001 12:00:00 AM" && paginationView.ToDate.ToString() == "1/1/0001 12:00:00 AM")
            {
                paginationView.ToDate = null;
                paginationView.FromDate = null;
            }
            var paginatedBooksModel = new PaginatedBooksModel
            {
                PageNumber = paginationView.CurrentPage,
                PageSize = paginationView.PageSize,
                AuthorsId = paginationView.AuthorsId,
                GenresId = paginationView.GenresId,
                MinPrice = paginationView.MinPrice,
                MaxPrice = paginationView.MaxPrice,
                FromDate = paginationView.FromDate,
                ToDate = paginationView.ToDate
            };
            int booksCount = await _bookRepository.GetPaginatedBooksCount(paginatedBooksModel);
            IEnumerable<Book> paginatedBooks = await _bookRepository.GetPaginatedBooks(paginatedBooksModel);
            IEnumerable<AuthorBook> authorBooks = await _authorBookRepository.GetIncludedElements();
            IEnumerable<BookGenre> bookGenres = await _bookGenreRepository.GetIncludedlements();
            var bookList = new List<Book>();
            if (paginationView.FromDate != null && paginationView.ToDate != null)
            {
                bookList = paginatedBooks.Where(book => DateParser(book.PublishDate) <= paginationView.ToDate && DateParser(book.PublishDate) >= paginationView.FromDate)
                    .Skip((paginationView.CurrentPage - 1) * paginationView.PageSize)
                    .Take(paginationView.PageSize).ToList();
                paginatedBooks = bookList;
                booksCount = bookList.Count;
            }
            var result = new GetPaginatedBooksView
            {
                Books = paginatedBooks.Select(book => MapPaginatedBook(book, authorBooks.Where(authorBook => authorBook.Book.Id == book.Id).ToList(),
                bookGenres.Where(bookGenre => bookGenre.Book.Id == book.Id).ToList())).ToList(),
                TotalItems = booksCount,
                PageSize = paginationView.PageSize,
                CurrentPage = paginationView.CurrentPage
            };
            result.TotalPages = (int)Math.Ceiling((decimal)result.TotalItems / result.PageSize);
            return result;
        }
        public async Task<int> GetMaxPrice()
        {
            return await _bookRepository.GetMaxPriceAsync();
        }
        public async Task Delete(string id)
        {
            await _unitOfWork.Books.Delete(id);
        }
        private BookGetPaginatedBooksViewItem MapPaginatedBook(Book book, List<AuthorBook> authorBooks, List<BookGenre> bookGenres)
        {
            var result = new BookGetPaginatedBooksViewItem();
            result.Id = book.Id;
            result.Price = book.Price;
            result.Title = book.Title;
            result.PublishDate = DateParser(book.PublishDate);
            result.ImageUrl = book.ImageUrl;
            result.Description = book.Description;
            result.Authors = authorBooks.Select(author => new AuthorGetPaginatedBooksViewItem
            {
                Id = author.Author.Id,
                Name = author.Author.Name
            }).ToList();
            result.Genres = bookGenres.Select(genre => new GenreGetPaginatedBooksViewItem
            {
                Id = genre.Genre.Id,
                Name = genre.Genre.Name
            }).ToList();
            return result;
        }
        private BookGetBooksViewItem MapBook(Book book, List<AuthorBook> authorBooks, List<BookGenre> bookGenres)
        {
            var result = new BookGetBooksViewItem();
            result.Id = book.Id;
            result.Price = book.Price;
            result.Title = book.Title;
            result.PublishDate = DateParser(book.PublishDate);
            result.ImageUrl = book.ImageUrl;
            result.Description = book.Description;
            result.Authors = authorBooks.Select(author => new AuthorGetBooksViewItem
            {
                Id = author.Author.Id,
                Name = author.Author.Name
            }).ToList();
            result.Genres = bookGenres.Select(genre => new GenreGetBooksViewItem
            {
                Id = genre.Genre.Id,
                Name = genre.Genre.Name
            }).ToList();
            return result;
        }
        private DateTime DateParser(string date)
        {
            var outDate = new DateTime();
            if (!DateTime.TryParse(date, out outDate))
            {
                throw new ArgumentException("Date is not valid");
            }
            return outDate;
        }
    }
}
