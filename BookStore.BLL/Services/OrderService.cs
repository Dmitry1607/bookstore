﻿using System.Collections.Generic;
using BookStore.DAL.Interfaces;
using BookStore.BLL.Views;
using BookStore.DAL.Entities;
using BookStore.BLL.Interfaces;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using Stripe;
using Order = BookStore.DAL.Entities.Order;

namespace BookStore.BLL.Services
{
    public class OrderService : IOrderService
    {
        private readonly UserManager<User> _userManager;
        private readonly IBookOrderRepository _bookOrderRepository;
        private readonly IMagazineOrderRepository _magazineOrderRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IBookRepository _bookRepository;
        private readonly IMagazineRepository _magazineRepository;
        private readonly TokenService _tokenService;
        private readonly ChargeService _chargeService;
        public OrderService(UserManager<User> userManager,
            IBookOrderRepository bookOrderRepository,
            IMagazineOrderRepository magazineOrderRepository,
            IOrderRepository orderRepository,
            IBookRepository bookRepository,
            IMagazineRepository magazineRepository)
        {
            _userManager = userManager;
            _bookOrderRepository = bookOrderRepository;
            _magazineOrderRepository = magazineOrderRepository;
            _orderRepository = orderRepository;
            _bookRepository = bookRepository;
            _magazineRepository = magazineRepository;
            _tokenService = new TokenService();
            _chargeService = new ChargeService();
        }
        public async Task<bool> Charge(PaymentView paymentView)
        {
            IEnumerable<string> booksIds = paymentView.User.OrderedBooks.Select(book => book.Id);
            IEnumerable<string> magazinesIds = paymentView.User.OrderedMagazines.Select(magazine => magazine.Id);
            IEnumerable<Book> books = await _bookRepository.Get(booksIds.ToList());
            IEnumerable<Magazine> magazines = await _magazineRepository.GetElements(magazinesIds.ToList());
            int booksAmount = books.Select(book => book.Price).Sum();
            int magazinesAmount = magazines.Select(magazine => magazine.Price).Sum();
            paymentView.User.Amount = booksAmount + magazinesAmount;
            Token token = _tokenService.Create(new TokenCreateOptions
            {
                Card = new CreditCardOptions
                {
                    Number = paymentView.CardNumber,
                    ExpYear = paymentView.CardExpYear,
                    ExpMonth = paymentView.CardExpMonth,
                    Cvc = paymentView.CardCvc
                }
            });
            Charge charge = _chargeService.Create(new ChargeCreateOptions
            {
                Amount = paymentView.User.Amount,
                Currency = "usd",
                Description = "Book Store test payment",
                Source = token.Id
            });
            if (!(charge.Status == "succeeded"))
            {
                return false;
            }
            User user = await _userManager.FindByEmailAsync(paymentView.User.Email);
            var order = new Order
            {
                Amount = paymentView.User.Amount,
                UserId = user.Id
            };
            await _orderRepository.Create(order);
            var magazineOrderList = paymentView.User.OrderedMagazines.Select(magazineId => new MagazineOrder
            {
                OrderId = order.Id,
                MagazineId = magazineId.Id
            }).ToList();
            var bookOrderList = paymentView.User.OrderedBooks.Select(bookId => new BookOrder
            {
                OrderId = order.Id,
                BookId = bookId.Id
            }).ToList();
            await _bookOrderRepository.AddRange(bookOrderList);
            await _magazineOrderRepository.AddRange(magazineOrderList);
            return true;
        }
    }
}
