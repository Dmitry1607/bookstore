﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using BookStore.DAL.Entities;
using System.Threading.Tasks;
using BookStore.BLL.Views;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using BookStore.BLL.Interfaces;
using BookStore.DAL.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Text;
using BookStore.BLL.Views.UserViews;
using BookStore.DAL.Models;

namespace BookStore.BLL.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private IConfiguration _configuration { get; }
        private readonly IUserRepository _userRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IBookOrderRepository _bookOrderRepository;
        private readonly IMagazineOrderRepository _magazineOrderRepository;
        private readonly IAuthorBookRepository _authorBookRepository;
        private readonly IBookGenreRepository _bookGenreRepository;
        private readonly IMagazineBookRepository _magazineBookRepository;
        private readonly IEmailService _emailService;

        public AccountService(UserManager<User> userManager,
            SignInManager<User> signInManager,
            IUserRepository userRepository,
            IOrderRepository orderRepository,
            IBookOrderRepository bookOrderRepository,
            IMagazineOrderRepository magazineOrderRepository,
            IAuthorBookRepository authorBookRepository,
            IBookGenreRepository bookGenreRepository,
            IMagazineBookRepository magazineBookRepository,
            IConfiguration configuration,
            IEmailService emailService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _userRepository = userRepository;
            _orderRepository = orderRepository;
            _bookOrderRepository = bookOrderRepository;
            _magazineOrderRepository = magazineOrderRepository;
            _authorBookRepository = authorBookRepository;
            _bookGenreRepository = bookGenreRepository;
            _magazineBookRepository = magazineBookRepository;
            _configuration = configuration;
            _emailService = emailService;
        }
        public async Task Register(RegisterUserView model)
        {
            string url = _configuration["ConfirmEmailUrl"];
            var user = new User { Email = model.Email, UserName = model.Email };
            IdentityResult result = await _userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                throw new ArgumentException("Wrong credentials");
            }
            await _userManager.AddToRoleAsync(user, "user");
            string token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            string confirmingUrl = url + $"?email={model.Email}&token={token}";
            await _emailService.SendEmailAsync(model.Email, "Confirm your account", $"Confirm registration by this link: <a href={confirmingUrl}>link</a>");
        }
        public async Task<TokenView> Login(LoginView model)
        {
            SignInResult result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: true);
            User user = await _userManager.FindByNameAsync(model.Email);
            string role = (await _userManager.GetRolesAsync(user)).FirstOrDefault();
            if (!await _userManager.IsEmailConfirmedAsync(user))
            {
                throw new ArgumentException("Email isn't confirmed", "");
            }
            if (!result.Succeeded)
            {
                throw new ArgumentException("Incorrect login or pass!", "");
            }
            var claims = new[]
            {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, role)
            };
            var token = new TokenView
            {
                AccessToken = CreateAccessToken(claims),
                RefreshToken = CreateRefreshToken()
            };
            return token;
        }
        public async Task<TokenView> ExternalLogin(ExternalLoginView externalUser)
        {
            if (string.IsNullOrEmpty(externalUser.Email))
            {
                throw new ArgumentException("Model is empty");
            }
            User user = await _userManager.FindByNameAsync(externalUser.Email);
            if (user == null)
            {
                var newUser = new User { Email = externalUser.Email, UserName = externalUser.Email };
                await _userManager.CreateAsync(newUser);
                await _userManager.AddToRoleAsync(newUser, "user");
                newUser.EmailConfirmed = true;
                await _signInManager.SignInAsync(newUser, true, null);
                return CreateTokenExternalLogin(newUser.Email);
            }
            await _signInManager.SignInAsync(user, true, null);
            return CreateTokenExternalLogin(user.Email);
        }
        private TokenView CreateTokenExternalLogin(string email)
        {
            var claims = new[]
            {
            new Claim(ClaimsIdentity.DefaultNameClaimType, email),
            new Claim(ClaimsIdentity.DefaultRoleClaimType, "user")
            };
            var token = new TokenView
            {
                AccessToken = CreateAccessToken(claims),
                RefreshToken = CreateRefreshToken()
            };
            return token;
        }
        private string CreateAccessToken(IEnumerable<Claim> claims)
        {
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: _configuration["JwtIssuer"],
                    audience: _configuration["JwtAudience"],
                    notBefore: now,
                    claims: claims,
                    expires: now.Add(TimeSpan.FromMinutes(60)),
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["JwtKey"])), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }
        private string CreateRefreshToken()
        {
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: _configuration["JwtIssuer"],
                    audience: _configuration["JwtAudience"],
                    notBefore: now,
                    expires: now.Add(TimeSpan.FromDays(7)),
                    signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["JwtKey"])), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }

        public async Task<bool> ConfirmEmail(RegistrationConfirmView registrationConfirmView)
        {
            string token = registrationConfirmView.Code.Replace(" ", "+");
            if (string.IsNullOrEmpty(registrationConfirmView.Email) || string.IsNullOrEmpty(token))
            {
                return false;
            }
            User user = await _userManager.FindByNameAsync(registrationConfirmView.Email);
            if (user == null)
            {
                return false;
            }
            IdentityResult result = await _userManager.ConfirmEmailAsync(user, token);
            if (!result.Succeeded)
            {
                return false;
            }
            return true;
        }
        public TokenView RefreshTokens(RefreshTokenView model)
        {
            var claims = new[]
            {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, model.Email),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, model.Role)
            };
            var token = new TokenView
            {
                AccessToken = CreateAccessToken(claims),
                RefreshToken = CreateRefreshToken()
            };
            return token;
        }
        public async Task AddUser(AddUserView userView)
        {
            var user = new User
            {
                Email = userView.Email,
                UserName = userView.Email,
                EmailConfirmed = userView.EmailConfirmed
            };
            IdentityResult result = await _userManager.CreateAsync(user, userView.Password);
            if (!result.Succeeded)
            {
                throw new ArgumentException("User not added");
            }
            await _userManager.AddToRoleAsync(user, userView.Role);
        }
        public async Task UpdateUser(UpdateUserView model)
        {
            User user = await _userManager.FindByIdAsync(model.Id);
            if (user == null)
            {
                throw new ArgumentException("User not found");
            }
            if (string.IsNullOrEmpty(model.Role.FirstOrDefault()))
            {
                throw new ArgumentException("Field role is empty");
            }
            if (string.IsNullOrEmpty(model.UserName))
            {
                throw new ArgumentException("Field username is empty");
            }
            user.EmailConfirmed = model.EmailConfirmed;
            string oldRole = (await _userManager.GetRolesAsync(user)).FirstOrDefault();
            await _userManager.RemoveFromRoleAsync(user, oldRole);
            await _userManager.AddToRoleAsync(user, model.Role.FirstOrDefault());
            user.UserName = model.UserName;
            await _userManager.UpdateAsync(user);
        }
        public async Task<GetUsersView> GetUsers()
        {
            IEnumerable<AspNetUserRole> userRoles = await _userRepository.GetUsersWithRoles();
            IEnumerable<User> users = await _userRepository.Get();
            var result = new GetUsersView();
            result.Users.AddRange(users.Select(user => MapUserRoles(user, userRoles.ToList())));
            return result;
        }
        public async Task<GetUserPurchasesView> GetUserPurchases(string email)
        {
            User user = await _userManager.FindByEmailAsync(email);
            IEnumerable<Order> userOrders = await _orderRepository.GetByUserId(user.Id);
            IEnumerable<BookOrder> allBookOrders = await _bookOrderRepository.GetIncludedElements();
            IEnumerable<MagazineOrder> allMagazineOrders = await _magazineOrderRepository.GetIncludedElements();
            IEnumerable<AuthorBook> allAuthorBooks = await _authorBookRepository.GetIncludedElements();
            IEnumerable<BookGenre> allBookGenres = await _bookGenreRepository.GetIncludedlements();
            IEnumerable<MagazineBook> allMagazineBooks = await _magazineBookRepository.GetIncludedElements();
            var purchases = userOrders.Select(order => new OrderGetUserPurchasesViewItem()
            {
                OrderId = order.Id,
                CreationDate = order.CreationDate.ToString(),
                Books = allBookOrders.Where(bookOrder => bookOrder.OrderId == order.Id)
                .Select(bookOrder => MapUserBook(bookOrder, allAuthorBooks.Where(authorBook => authorBook.Book.Id == bookOrder.Id).ToList(),
                allBookGenres.Where(bookGenre => bookGenre.Book.Id == bookOrder.Id).ToList())).ToList(),
                Magazines = allMagazineOrders.Where(magazineOrder => magazineOrder.OrderId == order.Id)
                .Select(magazineOrder => MapUserMagazine(magazineOrder, allMagazineBooks.Where(magazineBook => magazineBook.Book.Id == magazineOrder.Id).ToList())).ToList()
            }).ToList();
            var result = new GetUserPurchasesView()
            {
                UserId = user.Id,
                Purchases = purchases
            };
            return result;
        }
        public async Task<GetAllUsersPurchasesView> GetAllUsersPurchases()
        {
            IEnumerable<BookOrder> allBookOrders = await _bookOrderRepository.GetIncludedElements();
            IEnumerable<MagazineOrder> allMagazineOrders = await _magazineOrderRepository.GetIncludedElements();
            IEnumerable<AuthorBook> allAuthorBooks = await _authorBookRepository.GetIncludedElements();
            IEnumerable<BookGenre> allBookGenres = await _bookGenreRepository.GetIncludedlements();
            IEnumerable<MagazineBook> allMagazineBooks = await _magazineBookRepository.GetIncludedElements();
            IEnumerable<User> users = await _userRepository.GetUsersIncluded();
            var purchases = users.Select(user => new UserGetAllUsersPurchasesViewItem()
            {
                UserId = user.Id,
                Email = user.Email,
                Purchases = user.Orders.Where(order => order.UserId == user.Id).Select(order => new OrderGetAllUsersPurchasesViewItem()
                {
                    OrderId = order.Id,
                    CreationDate = order.CreationDate.ToString(),
                    Books = allBookOrders.Where(bookOrder => bookOrder.OrderId == order.Id)
                    .Select(bookOrder => MapAllUsersBook(bookOrder, allAuthorBooks.Where(authorBook => authorBook.Book.Id == bookOrder.Id).ToList(),
                    allBookGenres.Where(bookGenre => bookGenre.Book.Id == bookOrder.Id).ToList())).ToList(),
                    Magazines = allMagazineOrders.Where(magazineOrder => magazineOrder.OrderId == order.Id)
                    .Select(magazineOrder => MapAllUsersMagazine(magazineOrder,
                    allMagazineBooks.Where(magazineBook => magazineBook.Book.Id == magazineOrder.Id).ToList())).ToList()
                }).ToList()
            }).ToList();
            var result = new GetAllUsersPurchasesView()
            {
                UsersPurchases = purchases
            };
            return result;
        }
        public async Task DeleteUser(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                throw new ArgumentException("User not found");
            }
            await _userManager.DeleteAsync(user);
        }
        private BookGetUserPurchasesViewItem MapUserBook(BookOrder bookOrder, List<AuthorBook> authorBooks, List<BookGenre> bookGenres)
        {
            var result = new BookGetUserPurchasesViewItem();
            result.Id = bookOrder.BookId;
            result.Price = bookOrder.Book.Price;
            result.Title = bookOrder.Book.Title;
            result.Description = bookOrder.Book.Description;
            result.ImageUrl = bookOrder.Book.ImageUrl;
            result.PublishDate = DateParser(bookOrder.Book.PublishDate);
            result.Authors = authorBooks.Select(author => new AuthorGetUserBooksViewItem
            {
                Id = author.Author.Id,
                Name = author.Author.Name
            }).ToList();
            result.Genres = bookGenres.Select(genre => new GenreGetUserBooksViewItem
            {
                Id = genre.Genre.Id,
                Name = genre.Genre.Name
            }).ToList();
            return result;
        }
        private MagazineGetUserPurchasesViewItem MapUserMagazine(MagazineOrder magazineOrder, List<MagazineBook> magazineBooks)
        {
            var res = new MagazineGetUserPurchasesViewItem();
            res.Id = magazineOrder.MagazineId;
            res.Price = magazineOrder.Magazine.Price;
            res.Title = magazineOrder.Magazine.Title;
            res.Books = magazineBooks.Select(book => new BookGetUserMagazinesViewItem
            {
                Id = book.Book.Id,
                Title = book.Book.Title,
                Price = book.Book.Price
            }).ToList();
            return res;
        }
        private BookGetAllUsersPurchasesViewItem MapAllUsersBook(BookOrder bookOrder, List<AuthorBook> authorBooks, List<BookGenre> bookGenres)
        {
            var result = new BookGetAllUsersPurchasesViewItem();
            result.Id = bookOrder.BookId;
            result.Price = bookOrder.Book.Price;
            result.Title = bookOrder.Book.Title;
            result.Description = bookOrder.Book.Description;
            result.ImageUrl = bookOrder.Book.Description;
            result.PublishDate = DateParser(bookOrder.Book.PublishDate);
            result.Authors = authorBooks.Select(author => new AuthorGetAllUsersBooksViewItem
            {
                Id = author.Author.Id,
                Name = author.Author.Name
            }).ToList();
            result.Genres = bookGenres.Select(genre => new GenreGetAllUsersBooksViewItem
            {
                Id = genre.Genre.Id,
                Name = genre.Genre.Name
            }).ToList();
            return result;
        }
        private MagazineGetAllUsersPurchasesViewItem MapAllUsersMagazine(MagazineOrder magazineOrder, List<MagazineBook> magazineBooks)
        {
            var res = new MagazineGetAllUsersPurchasesViewItem();
            res.Id = magazineOrder.MagazineId;
            res.Price = magazineOrder.Magazine.Price;
            res.Title = magazineOrder.Magazine.Title;
            res.Books = magazineBooks.Select(book => new BookGetAllUsersMagazinesViewItem
            {
                Id = book.Book.Id,
                Title = book.Book.Title,
                Price = book.Book.Price
            }).ToList();
            return res;
        }
        private UserGetUsersViewItem MapUserRoles(User user, List<AspNetUserRole> userRoles)
        {
            var result = new UserGetUsersViewItem();
            result.Id = user.Id;
            result.Email = user.Email;
            result.EmailConfirmed = user.EmailConfirmed;
            result.UserName = user.UserName;
            result.Role = userRoles.FirstOrDefault(userRole => userRole.UserId == user.Id).Role.Select(role => role.Name).ToList();
            return result;
        }
        private DateTime DateParser(string date)
        {
            var outDate = new DateTime();
            if (!DateTime.TryParse(date, out outDate))
            {
                throw new ArgumentException("Date is not valid");
            }
            return outDate;
        }

    }
}
