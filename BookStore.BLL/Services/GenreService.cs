﻿using BookStore.BLL.Interfaces;
using BookStore.BLL.Views;
using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace BookStore.BLL.Services
{
    public class GenreService : IGenreService
    {
        private readonly IUnitOfWork _unitOfWork;
        public GenreService(IUnitOfWork uow)
        {
            _unitOfWork = uow;
        }
        public async Task Add(AddGenreView model)
        {
            var genre = new Genre
            {
                Name = model.Name
            };
            await _unitOfWork.Genres.Create(genre);
        }
        public async Task<GetGenresView> GetAll()
        {
            IEnumerable<Genre> genres = (await _unitOfWork.Genres.Get());
            var result = new GetGenresView
            {
                Genres = genres.Select(genre => new GetGenreViewItem
                {
                    Id = genre.Id,
                    Name = genre.Name
                }).ToList()
            };
            return result;
        }
        public async Task Delete(string id)
        {
            await _unitOfWork.Genres.Delete(id);
        }
        public async Task Update(UpdateGenreView model)
        {
            Genre genre = await _unitOfWork.Genres.Get(model.Id);
            if (genre == null)
            {
                throw new ArgumentException("Genre not found");
            }
            genre.Name = model.Name;
            await _unitOfWork.Genres.Update(genre);
        }
    }
}
