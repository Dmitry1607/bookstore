﻿using System.Collections.Generic;
using BookStore.DAL.Interfaces;
using BookStore.BLL.Views;
using BookStore.DAL.Entities;
using System.ComponentModel.DataAnnotations;
using BookStore.BLL.Interfaces;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace BookStore.BLL.Services
{
    public class MagazineService : IMagazineService
    {
        private readonly IMagazineBookRepository _magazineBookRepository;

        private readonly IUnitOfWork _unitOfWork;
        public MagazineService(IUnitOfWork uow, IMagazineBookRepository magazineBookRepository)
        {
            _unitOfWork = uow;
            _magazineBookRepository = magazineBookRepository;
        }
        public async Task Add(AddMagazineView magazineView)
        {
            var magazine = new Magazine
            {
                Title = magazineView.Title,
                Price = magazineView.Price
            };
            await _unitOfWork.Magazines.Create(magazine);
            var magazineBookList = magazineView.MagazineBookList.Select(bookId => new MagazineBook()
            {
                MagazineId = magazine.Id,
                BookId = bookId
            }).ToList();
            await _magazineBookRepository.AddRange(magazineBookList);
        }
        public async Task Update(UpdateMagazineView magazineView)
        {
            Magazine magazine = await _unitOfWork.Magazines.Get(magazineView.Id);
            if (magazine == null)
            {
                throw new ArgumentException("Magazine not found");
            }
            magazine.Title = magazineView.Title;
            magazine.Price = magazineView.Price;
            await _unitOfWork.Magazines.Update(magazine);
            await _magazineBookRepository.DeleteById(magazine.Id);
            var magazineBookList = magazineView.MagazineBookList.Select(bookId => new MagazineBook()
            {
                MagazineId = magazine.Id,
                BookId = bookId
            }).ToList();
            await _magazineBookRepository.AddRange(magazineBookList);
        }
        public async Task<GetMagazinesView> GetAll()
        {
            IEnumerable<Magazine> magazines = await _unitOfWork.Magazines.Get();
            IEnumerable<MagazineBook> magazineBooks = await _magazineBookRepository.GetIncludedElements();
            var result = new GetMagazinesView
            {
                Magazines = magazines.Select(magazine => MapMagazine(magazine, magazineBooks.Where(magazineBook => magazineBook.Magazine.Id == magazine.Id).ToList())).ToList()
            };
            return result;
        }
        public async Task<GetMagazineView> Get(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ValidationException("Id is incorrect");
            }
            Magazine magazine = await _unitOfWork.Magazines.Get(id);
            if (magazine == null)
            {
                throw new ValidationException("Magazine is not found");
            }
            magazine.MagazineBooks = (await _magazineBookRepository.GetByMagazineId(magazine.Id)).ToList();
            var result = new GetMagazineView
            {
                Id = magazine.Id,
                Price = magazine.Price,
                Title = magazine.Title,
                Books = magazine.MagazineBooks.Where(magazineBook => magazineBook.Magazine.Id == magazine.Id).Select(magazineBook => new BookGetMagazineViewItem()
                {
                    Id = magazineBook.Book.Id,
                    Price = magazineBook.Book.Price,
                    PublishDate = DateTime.Parse(magazineBook.Book.PublishDate),
                    Title = magazineBook.Book.Title
                }).ToList()
            };
            return result;
        }
        public async Task Delete(string id)
        {
            await _unitOfWork.Magazines.Delete(id);
        }
        private MagazineGetMagazinesViewItem MapMagazine(Magazine magazine, List<MagazineBook> magazineBooks)
        {
            var res = new MagazineGetMagazinesViewItem();
            res.Id = magazine.Id;
            res.Price = magazine.Price;
            res.Title = magazine.Title;
            res.Books = magazineBooks.Select(book => new BookGetMagazinesViewItem
            {
                Id = book.Book.Id,
                Title = book.Book.Title,
                Price = book.Book.Price,
                PublishDate = DateParser(book.Book.PublishDate),
            }).ToList();
            return res;
        }
        private DateTime DateParser(string date)
        {
            var outDate = new DateTime();
            if (!DateTime.TryParse(date, out outDate))
            {
                throw new ArgumentException("Date is not valid");
            }
            return outDate;
        }

    }
}
