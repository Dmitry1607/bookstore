﻿using System.Collections.Generic;
using BookStore.DAL.Interfaces;
using BookStore.BLL.Views;
using BookStore.DAL.Entities;
using System.ComponentModel.DataAnnotations;
using BookStore.BLL.Interfaces;
using System.Threading.Tasks;
using System.Linq;
using System;
using BookStore.BLL.Views.AuthorViews;

namespace BookStore.BLL.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;

        private readonly IUnitOfWork _unitOfWork;

        public AuthorService(IUnitOfWork uow, IAuthorRepository repo)
        {
            _unitOfWork = uow;
            _authorRepository = repo;
        }
        public async Task Add(AddAuthorView authorViewModel)
        {
            Author exAuthor = await _authorRepository.GetAuthorByName(authorViewModel.Name);
            if (exAuthor != null)
            {
                throw new ArgumentException("Author already exists");
            }
            var author = new Author
            {
                Name = authorViewModel.Name
            };
            await _authorRepository.Create(author);
        }
        public async Task<GetAuthorsView> GetAll()
        {
            IEnumerable<Author> authors = await _unitOfWork.Authors.Get();
            var result = new GetAuthorsView
            {
                Authors = authors.Select(author => new GetAuthorViewItem
                {
                    Id = author.Id,
                    Name = author.Name
                }).ToList()
            };
            return result;
        }
        public async Task<GetAuthorView> Get(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ValidationException("Id is incorrect");
            }
            Author author = await _unitOfWork.Authors.Get(id);
            if (author == null)
            {
                throw new ValidationException("Author is not found");
            }
            return new GetAuthorView { Id = author.Id, Name = author.Name };
        }
        public async Task Delete(string id)
        {
            await _unitOfWork.Authors.Delete(id);
        }
        public async Task Update(UpdateAuthorView model)
        {
            Author author = await _unitOfWork.Authors.Get(model.Id);
            if (author == null)
            {
                throw new ArgumentException("Author is not found");
            }
            author.Name = model.Name;
            await _unitOfWork.Authors.Update(author);
        }
    }
}
