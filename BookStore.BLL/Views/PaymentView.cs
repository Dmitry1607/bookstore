﻿using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class PaymentView
    {
        public string CardNumber{ get; set; }
        public int CardExpMonth { get; set; }
        public int CardExpYear { get; set; }
        public string CardCvc { get; set; }
        public UserPaymentView User { get; set; }
    }

    public class UserPaymentView
    {
        public string Email { get; set; }
        public int Amount { get; set; }
        public List<BookGetBooksViewItem> OrderedBooks { get; set; }
        public List<MagazineGetMagazinesViewItem> OrderedMagazines { get; set; }
    }
}
