﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views
{
    public class LoginView
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
