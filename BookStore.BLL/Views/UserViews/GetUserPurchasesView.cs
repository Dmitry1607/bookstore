﻿using System;
using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class GetUserPurchasesView
    {
        public string UserId { get; set; }
        public List<OrderGetUserPurchasesViewItem> Purchases { get; set; }
        public GetUserPurchasesView()
        {
            Purchases = new List<OrderGetUserPurchasesViewItem>();
        }
    }
    public class OrderGetUserPurchasesViewItem
    {
        public string OrderId { get; set; }
        public string CreationDate { get; set; }
        public List<BookGetUserPurchasesViewItem> Books { get; set; }
        public List<MagazineGetUserPurchasesViewItem> Magazines { get; set; }
        public OrderGetUserPurchasesViewItem()
        {
            Books = new List<BookGetUserPurchasesViewItem>();
            Magazines = new List<MagazineGetUserPurchasesViewItem>();
        }
    }

    public class MagazineGetUserPurchasesViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public List<BookGetUserMagazinesViewItem> Books { get; set; }
        public MagazineGetUserPurchasesViewItem()
        {
            Books = new List<BookGetUserMagazinesViewItem>();
        }
    }

    public class BookGetUserMagazinesViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
    }

    public class BookGetUserPurchasesViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public DateTime PublishDate { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public List<AuthorGetUserBooksViewItem> Authors { get; set; }
        public List<GenreGetUserBooksViewItem> Genres { get; set; }
        public BookGetUserPurchasesViewItem()
        {
            Authors = new List<AuthorGetUserBooksViewItem>();
            Genres = new List<GenreGetUserBooksViewItem>();
        }

    }

    public class GenreGetUserBooksViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class AuthorGetUserBooksViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
