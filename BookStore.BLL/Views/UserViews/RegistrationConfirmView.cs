﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BLL.Views.UserViews
{
    public class RegistrationConfirmView
    {
        public string Email { get; set; }
        public string Code { get; set; }
    }
}
