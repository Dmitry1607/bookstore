﻿using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class GetUsersView
    {
        public List<UserGetUsersViewItem> Users { get; set; }
        public GetUsersView()
        {
            Users = new List<UserGetUsersViewItem>();
        }
    }

    public class UserGetUsersViewItem
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public List<string> Role { get; set; } 
        public bool EmailConfirmed { get; set; }
        public UserGetUsersViewItem()
        {
            Role = new List<string>();
        }
    }
}
