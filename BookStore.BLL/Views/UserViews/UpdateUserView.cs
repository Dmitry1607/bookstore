﻿using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class UpdateUserView
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public List<string> Role { get; set; }
        public bool EmailConfirmed { get; set; }
    }
}
