﻿namespace BookStore.BLL.Views
{
    public class AddUserView
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool EmailConfirmed { get; set; }
    }
}
