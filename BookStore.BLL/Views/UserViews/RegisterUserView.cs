﻿using System.ComponentModel.DataAnnotations;

namespace BookStore.BLL.Views
{
    public class RegisterUserView
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [Compare("Password", ErrorMessage ="Passwords are incorrect")]
        public string PasswordConfirm { get; set; }
    }
}