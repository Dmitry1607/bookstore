﻿using System;
using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class GetAllUsersPurchasesView
    {
        public List<UserGetAllUsersPurchasesViewItem> UsersPurchases { get; set; }
        public GetAllUsersPurchasesView()
        {
            UsersPurchases = new List<UserGetAllUsersPurchasesViewItem>();
        }
    }

    public class UserGetAllUsersPurchasesViewItem
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        public List<OrderGetAllUsersPurchasesViewItem> Purchases { get; set; }
        public UserGetAllUsersPurchasesViewItem()
        {
            Purchases = new List<OrderGetAllUsersPurchasesViewItem>();
        }
    }

    public class OrderGetAllUsersPurchasesViewItem
    {
        public string OrderId { get; set; }
        public string CreationDate { get; set; }
        public List<BookGetAllUsersPurchasesViewItem> Books { get; set; }
        public List<MagazineGetAllUsersPurchasesViewItem> Magazines { get; set; }
        public OrderGetAllUsersPurchasesViewItem()
        {
            Books = new List<BookGetAllUsersPurchasesViewItem>();
            Magazines = new List<MagazineGetAllUsersPurchasesViewItem>();
        }
    }

    public class MagazineGetAllUsersPurchasesViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public List<BookGetAllUsersMagazinesViewItem> Books { get; set; }
        public MagazineGetAllUsersPurchasesViewItem()
        {
            Books = new List<BookGetAllUsersMagazinesViewItem>();
        }
    }

    public class BookGetAllUsersMagazinesViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
    }

    public class BookGetAllUsersPurchasesViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public DateTime PublishDate { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public List<AuthorGetAllUsersBooksViewItem> Authors { get; set; }
        public List<GenreGetAllUsersBooksViewItem> Genres { get; set; }
        public BookGetAllUsersPurchasesViewItem()
        {
            Authors = new List<AuthorGetAllUsersBooksViewItem>();
            Genres = new List<GenreGetAllUsersBooksViewItem>();
        }
    }

    public class GenreGetAllUsersBooksViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class AuthorGetAllUsersBooksViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
