﻿namespace BookStore.BLL.Views
{
    public class BookFilterView
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
    }
}
