﻿using System;
using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class UpdateBookView
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public DateTime PublishDate { get; set; }
        public List<string> AuthorBookList { get; set; }
        public List<string> BookGenreList { get; set; }
    }
}
