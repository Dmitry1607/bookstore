﻿using System;
using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class GetBooksView
    {
        public List<BookGetBooksViewItem> Books { get; set; }
        public GetBooksView()
        {
            Books = new List<BookGetBooksViewItem>();
        }
    }

    public class BookGetBooksViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public DateTime PublishDate { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public List<AuthorGetBooksViewItem> Authors { get; set; }
        public List<GenreGetBooksViewItem> Genres { get; set; }
    }

    public class GenreGetBooksViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class AuthorGetBooksViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
