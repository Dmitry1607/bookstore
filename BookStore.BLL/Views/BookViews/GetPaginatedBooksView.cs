﻿using System;
using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class GetPaginatedBooksView
    {
        public int TotalPages { get; set; }
        public int TotalItems { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
        public List<BookGetPaginatedBooksViewItem> Books { get; set; }
        public GetPaginatedBooksView()
        {
            Books = new List<BookGetPaginatedBooksViewItem>();
        }
    }

    public class BookGetPaginatedBooksViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public DateTime PublishDate { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public bool Visible { get; set; }
        public List<AuthorGetPaginatedBooksViewItem> Authors { get; set; }
        public List<GenreGetPaginatedBooksViewItem> Genres { get; set; }
    }

    public class GenreGetPaginatedBooksViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class AuthorGetPaginatedBooksViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
