﻿using System;
using System.Collections.Generic;

namespace BookStore.BLL.Views
{

    public class GetBookView
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public DateTime PublishDate { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public List<AuthorGetBookViewItem> Authors { get; set; } 
        public List<GenreGetBookViewItem> Genres { get; set; }
    }

    public class GenreGetBookViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class AuthorGetBookViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    
}
