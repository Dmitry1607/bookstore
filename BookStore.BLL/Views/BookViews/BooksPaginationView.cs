﻿using System;
using System.Collections.Generic;

namespace BookStore.BLL.Views.BookViews
{
    public class BooksPaginationView
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<string> AuthorsId { get; set; }
        public List<string> GenresId { get; set; }
        public BooksPaginationView()
        {
            AuthorsId = new List<string>();
            GenresId = new List<string>();
            FromDate = new DateTime();
            ToDate = new DateTime();
        }
    }
}
