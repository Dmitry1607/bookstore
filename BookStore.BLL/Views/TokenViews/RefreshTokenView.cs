﻿namespace BookStore.BLL.Views
{
    public class RefreshTokenView
    {
        public string Email { get; set; }
        public string Role { get; set; }
    }
}
