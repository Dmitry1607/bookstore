﻿namespace BookStore.BLL.Views
{
    public class TokenView
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
