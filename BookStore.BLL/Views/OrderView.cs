﻿namespace BookStore.BLL.Views
{
    public class OrderView
    {
        public string Id { get; set; }
        public int Price { get; set; }
        public string UserId { get; set; }
    }
}
