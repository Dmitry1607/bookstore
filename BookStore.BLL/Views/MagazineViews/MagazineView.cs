﻿namespace BookStore.BLL.Views
{
    public class MagazineView
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
    }
}
