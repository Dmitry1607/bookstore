﻿namespace BookStore.BLL.Views
{
    public class MagazineOrderView
    {
        public string Id { get; set; }
        public string MagazineId { get; set; }
        public string OrderId { get; set; }
    }
}
