﻿using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class UpdateMagazineView
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public List<string> MagazineBookList { get; set; }
    }
}
