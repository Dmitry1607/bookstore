﻿using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class AddMagazineView
    {
        public string Title { get; set; }
        public int Price { get; set; }
        public List<string> MagazineBookList { get; set; }
    }
}
