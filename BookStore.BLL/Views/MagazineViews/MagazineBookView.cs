﻿namespace BookStore.BLL.Views
{
    public class MagazineBookView
    {
        public string Id { get; set; }
        public string MagazineId { get; set; }
        public string BookId { get; set; }
    }
}
