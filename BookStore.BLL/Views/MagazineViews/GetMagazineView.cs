﻿using System;
using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class GetMagazineView
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public List<BookGetMagazineViewItem> Books { get; set; }
    }

    public class BookGetMagazineViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public DateTime PublishDate { get; set; }
    }
}
