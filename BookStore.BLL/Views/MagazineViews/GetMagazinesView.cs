﻿using System;
using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class GetMagazinesView
    {
        public List<MagazineGetMagazinesViewItem> Magazines { get; set; }
        public GetMagazinesView()
        {
            Magazines = new List<MagazineGetMagazinesViewItem>();
        }
    }

    public  class MagazineGetMagazinesViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public List<BookGetMagazinesViewItem> Books { get; set; }
    }

    public class BookGetMagazinesViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public int Price { get; set; }
        public DateTime PublishDate { get; set; }
    }
}
