﻿namespace BookStore.BLL.Views
{
    public class GetAuthorView
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
