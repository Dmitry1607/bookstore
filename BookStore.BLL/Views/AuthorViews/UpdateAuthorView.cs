﻿namespace BookStore.BLL.Views
{
    public class UpdateAuthorView
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
