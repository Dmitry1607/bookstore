﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BLL.Views.AuthorViews
{
    public class GetAuthorsView
    {
        public List<GetAuthorViewItem> Authors { get; set; }
        public GetAuthorsView()
        {
            Authors = new List<GetAuthorViewItem>();
        }
    }

    public class GetAuthorViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
