﻿using System.Collections.Generic;

namespace BookStore.BLL.Views
{
    public class GetGenresView
    {
        public List<GetGenreViewItem> Genres { get; set; }
        public GetGenresView()
        {
            Genres = new List<GetGenreViewItem>();
        }
    }

    public class GetGenreViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
