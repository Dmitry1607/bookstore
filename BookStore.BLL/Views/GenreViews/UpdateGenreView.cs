﻿namespace BookStore.BLL.Views
{
    public class UpdateGenreView
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
