﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using BookStore.BLL.Interfaces;
using BookStore.BLL.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using BookStore.DAL;
using BookStore.DAL.Entities;
using BookStore.BLL.Helpers;
using BookStore.BLL.Helpers.HelpersInterfaces;

namespace BookStore.BLL
{
    public class Startup
    {
        public static void ConfigureServices(IServiceCollection services, string conn, IConfiguration configuration)
        {
            services.AddAuthorization(options =>
            {
                options.DefaultPolicy =
                     new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser()
                    .Build();
            });
            services.Configure<IdentityOptions>(options =>
            {
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 1;
                options.Lockout.AllowedForNewUsers = true;
            });
            services.Configure<EmailConfig>(configuration.GetSection("Email"));
            services.AddIdentity<User, IdentityRole>().AddEntityFrameworkStores<StoreContext>().AddDefaultTokenProviders();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IAuthorService, AuthorService>();
            services.AddScoped<IBookService, BookService>();
            services.AddScoped<IMagazineService, MagazineService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IGenreService, GenreService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IUploadImageHelper, UploadImageHelper>();

            BookStore.DAL.Startup.ConfigureServices(services, conn, configuration);
        }
    }
}
