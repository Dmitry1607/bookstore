﻿using System.Threading.Tasks;
using BookStore.BLL.Views;
using BookStore.BLL.Views.AuthorViews;

namespace BookStore.BLL.Interfaces
{
    public interface IAuthorService
    {
        Task Add(AddAuthorView author);
        Task<GetAuthorView> Get(string id);
        Task<GetAuthorsView> GetAll();
        Task Delete(string id);
        Task Update(UpdateAuthorView model);
    }
}
