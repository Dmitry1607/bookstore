﻿using System.Threading.Tasks;
using BookStore.BLL.Views;

namespace BookStore.BLL.Interfaces
{
    public interface IMagazineService
    {
        Task Add(AddMagazineView magazine);
        Task Update(UpdateMagazineView magazineView);
        Task<GetMagazineView> Get(string id);
        Task<GetMagazinesView> GetAll();
        Task Delete(string id);
    }
}
