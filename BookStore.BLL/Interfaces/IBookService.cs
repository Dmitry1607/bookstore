﻿using System.Threading.Tasks;
using BookStore.BLL.Views;
using BookStore.BLL.Views.BookViews;
using Microsoft.AspNetCore.Http;

namespace BookStore.BLL.Interfaces
{
    public interface IBookService
    {
        Task<string> UploadImage(IFormFile file);
        Task Add(AddBookView book);
        Task Update(UpdateBookView bookView);
        Task Delete(string id);
        Task<GetBookView> Get(string id);
        Task<GetBooksView> GetAll();
        Task<GetPaginatedBooksView> GetPaginatedBooks(BooksPaginationView paginationView);
        Task<int> GetMaxPrice();
    }
}
