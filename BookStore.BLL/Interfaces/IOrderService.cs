﻿using System.Threading.Tasks;
using BookStore.BLL.Views;

namespace BookStore.BLL.Interfaces
{
    public interface IOrderService
    {
        Task<bool> Charge(PaymentView paymentView);
    }
}
