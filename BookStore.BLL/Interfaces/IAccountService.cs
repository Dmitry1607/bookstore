﻿using BookStore.BLL.Views;
using BookStore.BLL.Views.UserViews;
using System.Threading.Tasks;

namespace BookStore.BLL.Interfaces
{
    public interface IAccountService
    {
        Task Register(RegisterUserView model);
        Task<TokenView> Login(LoginView model);
        Task<TokenView> ExternalLogin(ExternalLoginView externalUser);
        Task<bool> ConfirmEmail(RegistrationConfirmView registrationConfirmView);
        TokenView RefreshTokens(RefreshTokenView model);
        Task AddUser(AddUserView userView);
        Task UpdateUser(UpdateUserView model);
        Task<GetUsersView> GetUsers();
        Task DeleteUser(string email);
        Task<GetUserPurchasesView> GetUserPurchases(string id);
        Task<GetAllUsersPurchasesView> GetAllUsersPurchases();
    }
}
