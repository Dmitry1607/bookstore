﻿using BookStore.BLL.Views;
using System.Threading.Tasks;

namespace BookStore.BLL.Interfaces
{
    public interface IGenreService
    {
        Task Add(AddGenreView model);
        Task<GetGenresView> GetAll();
        Task Delete(string id);
        Task Update(UpdateGenreView model);
    }
}
