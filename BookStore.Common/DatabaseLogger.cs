﻿using Dapper;
using Microsoft.Extensions.Logging;
using System;
using System.Data.SqlClient;

namespace BookStore.API.Middlewares
{
    public class DatabaseLogger : ILogger
    {
        private string connStr;
        private object _lock = new object();
        public DatabaseLogger(string conn)
        {
            connStr = conn;
        }
        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return logLevel == LogLevel.Error;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (formatter != null)
            {
                lock (_lock)
                {
                    using (SqlConnection _connection = new SqlConnection(connStr))
                    {
                        _connection.Open();
                        string sqlQuery = "insert into ErrorLogs (Id, CreationDate, Message, StackTrace) values(@Id, @CreationDate, @Message, @StackTrace)";
                        string str="";
                        if (exception!=null && exception.StackTrace != null)
                        {
                            str = exception.StackTrace;
                        }
                        _connection.Execute(sqlQuery, new { Id=Guid.NewGuid(), CreationDate= DateTime.Now, Message =formatter(state, exception).ToString(), StackTrace=str});
                    }
                }
            }
        }
    }
}
