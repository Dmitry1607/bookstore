﻿using Microsoft.Extensions.Logging;

namespace BookStore.API.Middlewares
{
    public static class DatabaseLoggerExtensions
    {
        public static ILoggerFactory AddFile(this ILoggerFactory factory, string conn)
        {
            factory.AddProvider(new DatabaseLoggerProvider(conn));
            return factory;
        }
    }
}
