﻿using Microsoft.Extensions.Logging;

namespace BookStore.API.Middlewares
{
    public class DatabaseLoggerProvider : ILoggerProvider
    {
        private string conn;
        public DatabaseLoggerProvider(string _conn)
        {
            conn = _conn;
        }
        public ILogger CreateLogger(string categoryName)
        {
            return new DatabaseLogger(conn);
        }

        public void Dispose()
        {
        }
    }
}
