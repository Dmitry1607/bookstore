using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BookStore.BLL.Interfaces;
using BookStore.BLL.Views;
using Microsoft.AspNetCore.Authorization;
using BookStore.BLL.Views.UserViews;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly IOrderService _orderService;

        public AccountController(IAccountService accService, IOrderService orderService)
        {
            _accountService = accService;
            _orderService = orderService;
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "user, admin")]
        public async Task<bool> Charge([FromBody]PaymentView paymentView)
        {
            bool result = await _orderService.Charge(paymentView);
            return result;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody]RegisterUserView model)
        {
            await _accountService.Register(model);
            return Ok();
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]LoginView model)
        {
            TokenView token = await _accountService.Login(model);
            return Ok(token);
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLogin([FromBody]ExternalLoginView user)
        {
            TokenView token = await _accountService.ExternalLogin(user);
            return Ok(token);
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<bool> ConfirmEmail([FromBody]RegistrationConfirmView registrationConfirmView)
        {
            return await _accountService.ConfirmEmail(registrationConfirmView);
        }
        [HttpPost]
        [AllowAnonymous]
        public IActionResult RefreshTokens([FromBody]RefreshTokenView model)
        {
            TokenView tokens = _accountService.RefreshTokens(model);
            return Ok(tokens);
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task AddUser([FromBody] AddUserView userView)
        {
            await _accountService.AddUser(userView);
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task UpdateUser([FromBody] UpdateUserView user)
        {
            await _accountService.UpdateUser(user);
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> GetUsers()
        {
            GetUsersView users = await _accountService.GetUsers();
            return Ok(users);
        }
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task DeleteUser(string id)
        {
            await _accountService.DeleteUser(id);
        }
        [HttpGet("{email}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> GetUserPurchases(string email)
        {
            GetUserPurchasesView purchases = await _accountService.GetUserPurchases(email);
            return Ok(purchases);
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> GetAllUsersPurchases()
        {
            GetAllUsersPurchasesView allPurchasses = await _accountService.GetAllUsersPurchases();
            return Ok(allPurchasses);
        }
    }
}
