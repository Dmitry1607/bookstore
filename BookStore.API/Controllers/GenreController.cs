﻿using System.Threading.Tasks;
using BookStore.BLL.Interfaces;
using BookStore.BLL.Views;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class GenreController : Controller
    {
        private readonly IGenreService _genreService;
        public GenreController(IGenreService service)
        {
            _genreService = service;
        }
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody]AddGenreView model)
        {
            await _genreService.Add(model);
            return Ok();
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin, user")]
        public async Task<IActionResult> GetAll()
        {
            GetGenresView genres = await _genreService.GetAll();
            return Ok(genres);
        }
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Delete(string id)
        {
            await _genreService.Delete(id);
            return Ok();
        }
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Update([FromBody]UpdateGenreView model)
        {
            await _genreService.Update(model);
            return Ok();
        }
    }
}
