using System.Threading.Tasks;
using BookStore.BLL.Interfaces;
using BookStore.BLL.Views;
using BookStore.BLL.Views.BookViews;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class BookController : Controller
    {
        private readonly IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> UploadImage(IFormFile file)
        {
            string url = await _bookService.UploadImage(file);
            return Json(url);
        }
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] AddBookView model)
        {
            await _bookService.Add(model);
            return Ok();
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Update([FromBody] UpdateBookView model)
        {
            await _bookService.Update(model);
            return Ok();
        }
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Delete(string id)
        {
            await _bookService.Delete(id);
            return Ok();
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> GetAll()
        {
            GetBooksView books = await _bookService.GetAll();
            return Ok(books);
        }
        [HttpGet("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Get(string id)
        {
            GetBookView book = await _bookService.Get(id);
            return Ok(book);
        }
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin, user")]
        public async Task<IActionResult> GetPaginatedBooks([FromBody]BooksPaginationView paginationView)
        {
            GetPaginatedBooksView books = await _bookService.GetPaginatedBooks(paginationView);
            return Ok(books);
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin, user")]
        public async Task<IActionResult> GetMaxPrice()
        {
            int maxPrice = await _bookService.GetMaxPrice();
            return Ok(maxPrice);
        }
    }
}
