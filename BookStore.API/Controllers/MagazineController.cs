﻿using System.Threading.Tasks;
using BookStore.BLL.Interfaces;
using BookStore.BLL.Views;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class MagazineController : Controller
    {
        private readonly IMagazineService _magazineService;

        public MagazineController(IMagazineService magazineService)
        {
            _magazineService = magazineService;
        }
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody] AddMagazineView model)
        {
            await _magazineService.Add(model);
            return Ok();
        }
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Update([FromBody] UpdateMagazineView model)
        {
            await _magazineService.Update(model);
            return Ok();
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin, user")]
        public async Task<IActionResult> GetAll()
        {
            GetMagazinesView magazines = await _magazineService.GetAll();
            return Ok(magazines);
        }
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Delete(string id)
        {
            await _magazineService.Delete(id);
            return Ok();
        }
    }
}
