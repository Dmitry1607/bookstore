using BookStore.BLL.Interfaces;
using BookStore.BLL.Views;
using BookStore.BLL.Views.AuthorViews;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace BookStore.API.Controllers
{


    [Route("api/[controller]/[action]")]
    public class AuthorController : Controller
    {
        private readonly IAuthorService _authorService;
        public AuthorController(IAuthorService service)
        {
            _authorService = service;
        }


        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Add([FromBody]AddAuthorView model)
        {
            await _authorService.Add(model);
            return Ok();
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin, user")]
        public async Task<IActionResult> GetAll()
        {
            GetAuthorsView authors = await _authorService.GetAll();
            return Ok(authors);
        }
        [HttpGet]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin, user")]
        public async Task<IActionResult> Get([FromBody]GetAuthorView model)
        {
            GetAuthorView author = await _authorService.Get(model.Id);
            return Ok(author);
        }
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        public async Task<IActionResult> Delete(string id)
        {
            await _authorService.Delete(id);
            return Ok();
        }
        [Authorize(AuthenticationSchemes = "Bearer", Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> Update([FromBody]UpdateAuthorView model)
        {
            await _authorService.Update(model);
            return Ok();
        }
    }
}
