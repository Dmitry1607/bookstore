﻿using Microsoft.Extensions.DependencyInjection;
using BookStore.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using BookStore.DAL.Interfaces;
using BookStore.DAL.Repositories.Dapper;
using BookStore.DAL.Repositories.Entity;
using BookStore.DAL.Repositories;
using Microsoft.Extensions.Configuration;

namespace BookStore.DAL
{
    public class Startup
    {
        public static void ConfigureServices(IServiceCollection services, string connection, IConfiguration configuration)
        {
            services.AddDbContext<StoreContext>(options => options.UseSqlServer(connection));
            string orm = configuration["Orm"];
            if (orm == OrmEnum.Dapper.ToString())
            {
                services.AddTransient<IAuthorBookRepository, AuthorBookRepository>();
                services.AddScoped<IAuthorRepository, AuthorRepository>();
                services.AddScoped<IBookOrderRepository, BookOrderRepository>();
                services.AddScoped<IMagazineBookRepository, MagazineBookRepository>();
                services.AddScoped<IMagazineOrderRepository, MagazineOrderRepository>();
                services.AddScoped<IMagazineRepository, MagazineRepository>();
                services.AddScoped<IOrderRepository, OrderRepository>();
                services.AddTransient<IBookGenreRepository, BookGenreRepository>();
                services.AddScoped<IGenreRepository, GenreRepository>();
                services.AddScoped<IBookRepository, BookRepository>();
                services.AddScoped<IUserRepository, UserRepository>();
            }
            if (orm == OrmEnum.Entity.ToString())
            {
                services.AddScoped<IAuthorBookRepository, AuthorBookRepositoryEF>();
                services.AddScoped<IAuthorRepository, AuthorRepositoryEF>();
                services.AddScoped<IBookRepository, BookRepositoryEF>();
                services.AddScoped<IBookOrderRepository, BookOrderRepositoryEF>();
                services.AddScoped<IMagazineBookRepository, MagazineBookRepositoryEF>();
                services.AddScoped<IMagazineOrderRepository, MagazineOrderRepositoryEF>();
                services.AddScoped<IMagazineRepository, MagazineRepositoryEF>();
                services.AddScoped<IOrderRepository, OrderRepositoryEF>();
                services.AddScoped<IGenreRepository, GenreRepositoryEF>();
                services.AddScoped<IBookGenreRepository, BookGenreRepositoryEF>();
                services.AddScoped<IUserRepository, UserRepositoryEF>();
            }
            services.AddScoped<IUnitOfWork, EFUnitOfWork>();
            var serviceProvider = services.BuildServiceProvider();
            DbInitializer.Initializer(serviceProvider.GetService<UserManager<User>>(), serviceProvider.GetService<RoleManager<IdentityRole>>(), serviceProvider.GetService<IConfiguration>());
        }
    }
}
