﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace BookStore.DAL.Entities
{

    public class Author : BaseEntity
    {
        public string Name { get; set; }
        [Write(false)]
        public virtual ICollection<AuthorBook> AuthorBooks { get; set; }
    }
}
