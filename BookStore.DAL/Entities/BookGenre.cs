﻿using Dapper.Contrib.Extensions;

namespace BookStore.DAL.Entities
{
    public class BookGenre : BaseEntity
    {
        public string BookId { get; set; }
        [Write(false)]
        public Book Book { get; set; }
        public string GenreId { get; set; }
        [Write(false)]
        public Genre Genre { get; set; }
    }
}
