﻿using Dapper.Contrib.Extensions;

namespace BookStore.DAL.Entities
{
    public class BookOrder : BaseEntity
    {
        public string BookId { get; set; }
        [Write(false)]
        public Book Book { get; set; }
        public string OrderId { get; set; }
        [Write(false)]
        public Order Order { get; set; }
    }
}
