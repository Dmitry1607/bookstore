﻿using Dapper.Contrib.Extensions;

namespace BookStore.DAL.Entities
{
    public class AuthorBook : BaseEntity
    {
        public string BookId { get; set; }
        [Write(false)]
        public Book Book { get; set; }
        public string AuthorId { get; set; }
        [Write(false)]
        public Author Author { get; set; }
    }
}
