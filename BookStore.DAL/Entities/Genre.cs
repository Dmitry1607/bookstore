﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace BookStore.DAL.Entities
{
    public class Genre : BaseEntity
    {
        public string Name { get; set; }
        [Write(false)]
        public virtual ICollection<BookGenre> BookGenres { get; set; }
    }
}
