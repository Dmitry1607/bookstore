﻿namespace BookStore.DAL.Entities
{
    public class ErrorLog : BaseEntity
    {
        public string Message { get; set; }
        public string StackTrace { get; set; }
    }
}
