﻿using Dapper.Contrib.Extensions;

namespace BookStore.DAL.Entities
{
    public class MagazineBook : BaseEntity
    {
        public string MagazineId { get; set; }
        [Write(false)]

        public Magazine Magazine { get; set; }
        public string BookId { get; set; }
        [Write(false)]

        public Book Book { get; set; }
    }
}
