﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace BookStore.DAL.Entities
{
    public class Magazine : BaseEntity
    {
        public string Title { get; set; }
        public int Price { get; set; }
        [Write(false)]
        public virtual ICollection<MagazineOrder> MagazineOrders { get; set; }
        [Write(false)]
        public virtual ICollection<MagazineBook> MagazineBooks { get; set; }
    }
}
