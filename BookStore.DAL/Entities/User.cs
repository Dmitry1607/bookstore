﻿using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace BookStore.DAL.Entities
{
    public class User : IdentityUser
    {
        public DateTime CreationDate { get; set; }
        public User()
        {
            CreationDate = DateTime.Now;
        }
        [Write(false)]
        public virtual ICollection<Order> Orders { get; set; }
    }
}
