﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace BookStore.DAL.Entities
{
    public class Book : BaseEntity
    {
        public string Title { get; set; }
        public int Price { get; set; }
        public string PublishDate { get; set; } 
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        [Write(false)]
        public virtual ICollection<BookOrder> BookOrders {get;set;}
        [Write(false)]
        public virtual ICollection<MagazineBook> MagazineBooks { get; set; }
        [Write(false)]
        public virtual ICollection<BookGenre> BookGenres { get; set; }
        [Write(false)]
        public virtual ICollection<AuthorBook> AuthorBooks { get; set; }
    }
}
