﻿using Dapper.Contrib.Extensions;
using System;

namespace BookStore.DAL.Entities
{
    public class BaseEntity
    {
        [ExplicitKey]
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public BaseEntity()
        {
            Id = Guid.NewGuid().ToString();
            CreationDate = DateTime.Now;
        }
    }
}
