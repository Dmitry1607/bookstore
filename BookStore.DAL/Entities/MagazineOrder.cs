﻿using Dapper.Contrib.Extensions;

namespace BookStore.DAL.Entities
{
    public class MagazineOrder : BaseEntity
    {
        public string MagazineId { get; set; }
        [Write(false)]

        public Magazine Magazine { get; set; }
        public string OrderId { get; set; }
        [Write(false)]

        public Order Order { get; set; }
    }
}
