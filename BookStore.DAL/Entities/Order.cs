﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace BookStore.DAL.Entities
{
    public class Order : BaseEntity
    {
        public int Amount { get; set; }
        public string UserId { get; set; }
        [Write(false)]
        public User User { get; set; }
        [Write(false)]
        public virtual ICollection<BookOrder> BookOrders { get; set; }
        [Write(false)]
        public virtual ICollection<MagazineOrder> MagazineOrders { get; set; }
    }
}
