﻿using BookStore.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace BookStore.DAL
{
    public class DbInitializer
    {
        public static async void Initializer(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            User user = await userManager.FindByEmailAsync(configuration["AdminEmail"]);
            if (user != null)
            {
                return;
            }
            await roleManager.CreateAsync(new IdentityRole("admin"));
            await roleManager.CreateAsync(new IdentityRole("user"));
            var admin = new User { Email = configuration["AdminEmail"], UserName = configuration["AdminEmail"] };
            string password = configuration["AdminPassword"];
            IdentityResult result = await userManager.CreateAsync(admin, password);
            admin.EmailConfirmed = true;
            if (result.Succeeded)
            {
                await userManager.AddToRoleAsync(admin, "admin");
            }

        }
    }
}
