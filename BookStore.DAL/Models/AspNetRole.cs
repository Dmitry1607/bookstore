﻿namespace BookStore.DAL.Models
{
    public class AspNetRole
    {
        public string Name { get; set; }
        public string RoleId { get; set; }
    }
}
