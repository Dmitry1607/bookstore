﻿using System.Collections.Generic;

namespace BookStore.DAL.Models
{
    public class AspNetUserRole
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
        public virtual ICollection<AspNetRole> Role { get; set; }
    }
}
