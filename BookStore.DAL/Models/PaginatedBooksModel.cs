﻿using System;
using System.Collections.Generic;

namespace BookStore.DAL.Models
{
    public class PaginatedBooksModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public List<string> AuthorsId { get; set; }
        public List<string> GenresId { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
