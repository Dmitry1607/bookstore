﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using BookStore.DAL.Entities;

namespace BookStore.DAL
{
    public class StoreContext : IdentityDbContext<User>
    {
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Magazine> Magazines { get; set; }
        public DbSet<BookGenre> BookGenres { get; set; }
        public DbSet<AuthorBook> AuthorBooks { get; set; }
        public DbSet<BookOrder> BookOrders { get; set; }
        public DbSet<MagazineBook> MagazineBooks { get; set; }
        public DbSet<MagazineOrder> MagazineOrders { get; set; }

        public DbSet<ErrorLog> ErrorLogs { get; set; }
        public StoreContext(DbContextOptions<StoreContext> options)
           : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AuthorBook>()
            .HasOne(authorBook => authorBook.Book)
            .WithMany(book => book.AuthorBooks)
            .HasForeignKey(authorBook => authorBook.BookId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<AuthorBook>()
            .HasOne(authorBook => authorBook.Author)
            .WithMany(author => author.AuthorBooks)
            .HasForeignKey(authorBook => authorBook.AuthorId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<BookGenre>()
            .HasOne(bookGenre => bookGenre.Book)
            .WithMany(book => book.BookGenres)
            .HasForeignKey(bookGenre => bookGenre.BookId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<BookGenre>()
            .HasOne(bookGenre => bookGenre.Genre)
            .WithMany(genre => genre.BookGenres)
            .HasForeignKey(bookGenre => bookGenre.GenreId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<MagazineBook>()
            .HasOne(magazineBook => magazineBook.Magazine)
            .WithMany(magazine => magazine.MagazineBooks)
            .HasForeignKey(magazineBook => magazineBook.MagazineId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<MagazineBook>()
            .HasOne(magazineBook => magazineBook.Book)
            .WithMany(book => book.MagazineBooks)
            .HasForeignKey(magazineBook => magazineBook.BookId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<BookOrder>()
            .HasOne(bookOrder => bookOrder.Book)
            .WithMany(book => book.BookOrders)
            .HasForeignKey(bookOrder => bookOrder.BookId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<BookOrder>()
            .HasOne(bookOrder => bookOrder.Order)
            .WithMany(order => order.BookOrders)
            .HasForeignKey(bookOrder => bookOrder.OrderId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
