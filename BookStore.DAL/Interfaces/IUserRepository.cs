﻿using BookStore.DAL.Entities;
using BookStore.DAL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DAL.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {
        Task<IEnumerable<AspNetUserRole>> GetUsersWithRoles();
        Task<IEnumerable<User>> GetUsersIncluded();
    }
}
