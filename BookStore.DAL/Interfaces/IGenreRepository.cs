﻿using BookStore.DAL.Entities;

namespace BookStore.DAL.Interfaces
{
    public interface IGenreRepository:IBaseRepository<Genre>
    {
    }
}
