﻿using System.Collections.Generic;
using System.Threading.Tasks;


namespace BookStore.DAL.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        Task Create(T el);
        Task Delete(string id);
        Task<T> Get(string id);
        Task<IEnumerable<T>> Get();
        Task Update(T el);
    }
}
