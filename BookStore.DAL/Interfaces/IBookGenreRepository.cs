﻿using BookStore.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DAL.Interfaces
{
    public interface IBookGenreRepository:IBaseRepository<BookGenre>
    {
        Task<IEnumerable<BookGenre>> GetByBookId(string id);
        Task DeleteByBookId(string id);
        Task AddRange(List<BookGenre> bookGenreList);
        Task RemoveRange(List<BookGenre> bookGenreList);
        Task<IEnumerable<BookGenre>> GetIncludedlements();
    }
}
