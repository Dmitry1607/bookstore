﻿using BookStore.DAL.Entities;
using System;

namespace BookStore.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IAuthorBookRepository AuthorBooks { get; }
        IAuthorRepository Authors { get; }
        IBookOrderRepository BookOrders { get; }
        IBookRepository Books { get; }
        IMagazineBookRepository MagazineBooks { get; }
        IMagazineOrderRepository MagazineOrders { get; }
        IMagazineRepository Magazines { get; }
        IOrderRepository Orders { get; }
        IUserRepository Users { get; }
        IGenreRepository Genres { get; }
        IBookGenreRepository BookGenres { get; }
    }
}
