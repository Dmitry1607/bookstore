﻿using BookStore.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DAL.Interfaces
{
    public interface IMagazineRepository : IBaseRepository<Magazine>
    {
        Task<IEnumerable<Magazine>> GetElements(List<string> ids);
    }
}
