﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.DAL.Entities;
using BookStore.DAL.Models;

namespace BookStore.DAL.Interfaces
{
    public interface IBookRepository : IBaseRepository<Book>
    {
        Task<IEnumerable<Book>> GetPaginatedBooks(PaginatedBooksModel paginatedBooksModel);
        Task<int> GetPaginatedBooksCount(PaginatedBooksModel paginatedBooksModel);
        Task<int> GetMaxPriceAsync();
        Task<IEnumerable<Book>> Get(List<string> ids);
    }
}
