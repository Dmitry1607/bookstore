﻿using BookStore.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DAL.Interfaces
{
    public interface IMagazineBookRepository : IBaseRepository<MagazineBook>
    {
        Task AddRange(List<MagazineBook> magazineBookList);
        Task RemoveRange(List<MagazineBook> magazineBookList);
        Task<IEnumerable<MagazineBook>> GetByMagazineId(string id);
        Task<IEnumerable<MagazineBook>> GetIncludedElements();
        Task DeleteById(string id);
    }
}
