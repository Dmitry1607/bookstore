﻿using BookStore.DAL.Entities;
using System.Threading.Tasks;

namespace BookStore.DAL.Interfaces
{
    public interface IAuthorRepository : IBaseRepository<Author>
    {
        Task<Author> GetAuthorByName(string name);
    }
}
