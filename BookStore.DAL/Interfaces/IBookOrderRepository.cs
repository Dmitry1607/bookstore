﻿using BookStore.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DAL.Interfaces
{
    public interface IBookOrderRepository : IBaseRepository<BookOrder>
    {
        Task AddRange(List<BookOrder> bookOrders);
        Task<IEnumerable<string>> GetBookOrdersByOrderId(string id);
        Task<IEnumerable<BookOrder>> GetIncludedElements();
    }
}
