﻿using BookStore.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DAL.Interfaces
{
    public interface IMagazineOrderRepository : IBaseRepository<MagazineOrder>
    {
        Task AddRange(IEnumerable<MagazineOrder> magazineOrders);
        Task<IEnumerable<string>> GetMagazineOrdersByOrderId(string id);
        Task<IEnumerable<MagazineOrder>> GetIncludedElements();
    }
}
