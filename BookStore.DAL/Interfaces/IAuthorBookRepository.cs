﻿using BookStore.DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DAL.Interfaces
{
    public interface IAuthorBookRepository : IBaseRepository<AuthorBook>
    {
        Task DeleteByBookId(string id);
        Task AddRange(List<AuthorBook> authorBookList);
        Task<IEnumerable<AuthorBook>> GetByBookId(string id);
        Task RemoveRange(List<AuthorBook> authorBookList);
        Task<IEnumerable<AuthorBook>> GetIncludedElements();
    }
}
