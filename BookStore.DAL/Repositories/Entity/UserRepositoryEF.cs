﻿using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using BookStore.DAL.Models;

namespace BookStore.DAL.Repositories.Entity
{
    public class UserRepositoryEF : BaseRepositoryEF<User>, IUserRepository
    {
        private readonly StoreContext _db;
        public UserRepositoryEF(StoreContext context) : base(context)
        {
            _db = context;
        }

        public async Task<IEnumerable<User>> GetUsersIncluded()
        {
            return _db.Users.Include(user => user.Orders);
        }

        public async Task<IEnumerable<AspNetUserRole>> GetUsersWithRoles()
        {
            var result = _db.UserRoles.Select(userRole => new AspNetUserRole
            {
                UserId = userRole.UserId,
                RoleId = userRole.RoleId,
                Role = _db.Roles.Where(role => role.Id == userRole.RoleId).Select(role => new AspNetRole
                {
                    RoleId = userRole.RoleId,
                    Name = role.Name
                }).ToList()
            });
            return result;
        }
    }
}
