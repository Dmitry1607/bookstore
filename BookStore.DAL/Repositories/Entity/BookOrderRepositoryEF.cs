﻿using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Entity
{
    public class BookOrderRepositoryEF : BaseRepositoryEF<BookOrder>, IBookOrderRepository
    {
        private readonly StoreContext _db;
        public BookOrderRepositoryEF(StoreContext context) : base(context)
        {
            _db = context;
        }
        public async Task AddRange(List<BookOrder> bookOrders)
        {
            await _db.BookOrders.AddRangeAsync(bookOrders);
            await _db.SaveChangesAsync();
        }
        public async Task<IEnumerable<BookOrder>> GetIncludedElements()
        {
            return _db.BookOrders.Include(bookOrder => bookOrder.Book).Include(bookOrder => bookOrder.Order);
        }
        public async Task<IEnumerable<string>> GetBookOrdersByOrderId(string id)
        {
            return _db.BookOrders.Where(bookOrder => bookOrder.OrderId == id).Select(bookOrder => bookOrder.BookId);
        }
    }
}
