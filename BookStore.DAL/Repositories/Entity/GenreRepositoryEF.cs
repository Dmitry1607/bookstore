﻿using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;

namespace BookStore.DAL.Repositories.Entity
{
    public class GenreRepositoryEF : BaseRepositoryEF<Genre>, IGenreRepository
    {
        private readonly StoreContext _db;
        public GenreRepositoryEF(StoreContext context) : base(context)
        {
            _db = context;
        }
    }
}
