﻿using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Entity
{
    public class OrderRepositoryEF : BaseRepositoryEF<Order>, IOrderRepository
    {
        private readonly StoreContext _db;
        public OrderRepositoryEF(StoreContext context) : base(context)
        {
            _db = context;
        }
        public async Task<IEnumerable<Order>> GetByUserId(string id)
        {
            return _db.Orders.Where(order => order.UserId == id);
        }
    }
}
