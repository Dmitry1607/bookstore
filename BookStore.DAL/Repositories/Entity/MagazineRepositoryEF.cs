﻿using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Entity
{
    public class MagazineRepositoryEF : BaseRepositoryEF<Magazine>, IMagazineRepository
    {
        private readonly StoreContext _db;
        public MagazineRepositoryEF(StoreContext context) : base(context)
        {
            _db = context;
        }
        public async Task<IEnumerable<Magazine>> GetElements(List<string> ids)
        {
            return _db.Magazines.Where(magazine => ids.Contains(magazine.Id));
        }
    }
}
