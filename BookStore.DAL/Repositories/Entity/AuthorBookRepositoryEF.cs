﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BookStore.DAL.Repositories.Entity
{
    public class AuthorBookRepositoryEF : BaseRepositoryEF<AuthorBook>, IAuthorBookRepository
    {
        private readonly StoreContext _db;
        public AuthorBookRepositoryEF(StoreContext context) : base(context)
        {
            _db = context;
        }
        public async Task<IEnumerable<AuthorBook>> GetByBookId(string id)
        {
            return await _db.AuthorBooks.Where(authorBook => authorBook.BookId == id).Include(authorBook => authorBook.Author).ToListAsync();
        }
        public async Task<IEnumerable<AuthorBook>> GetIncludedElements()
        {
            return await _db.AuthorBooks.Include(authorBook => authorBook.Book).Include(authorBook => authorBook.Author).ToListAsync();
        }
        public async Task AddRange(List<AuthorBook> authorBookList)
        {
            await _db.AuthorBooks.AddRangeAsync(authorBookList);
            await _db.SaveChangesAsync();
        }
        public async Task RemoveRange(List<AuthorBook> authorBookList)
        {
            _db.AuthorBooks.RemoveRange(authorBookList);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteByBookId(string id)
        {
            _db.AuthorBooks.RemoveRange(_db.AuthorBooks.Where(authorBook => authorBook.Id == id));
            await _db.SaveChangesAsync();
        }
    }
}
