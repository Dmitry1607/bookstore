﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using BookStore.DAL.Models;

namespace BookStore.DAL.Repositories.Entity
{
    public class BookRepositoryEF : BaseRepositoryEF<Book>, IBookRepository
    {
        private readonly StoreContext _context;
        public BookRepositoryEF(StoreContext context) : base(context)
        {
            _context = context;
        }

        public async Task<int> GetMaxPriceAsync()
        {
            return await _context.Books.MaxAsync(book => book.Price);
        }
        public async Task<IEnumerable<Book>> Get(List<string> ids)
        {
            return _context.Books.Where(book => ids.Contains(book.Id));
        }
        public async Task<int> GetPaginatedBooksCount(PaginatedBooksModel paginatedBooksModel)
        {
            var books = await _context.Books
                .Include(book => book.AuthorBooks)
                .Include(book => book.BookGenres)
                .Where(book => book.Price <= paginatedBooksModel.MaxPrice && book.Price >= paginatedBooksModel.MinPrice).ToListAsync();
            return books.Count;
        }

        public async Task<IEnumerable<Book>> GetPaginatedBooks(PaginatedBooksModel paginatedBooksModel)
        {
            var books = _context.Books
                .Include(book => book.AuthorBooks)
                .Include(book => book.BookGenres)
                .Where(book => book.Price <= paginatedBooksModel.MaxPrice && book.Price >= paginatedBooksModel.MinPrice);
            if (paginatedBooksModel.AuthorsId.Any())
            {
                books = books.Where(book => book.AuthorBooks.Any(authorBook => paginatedBooksModel.AuthorsId.Contains(authorBook.AuthorId)));
            }
            if (paginatedBooksModel.GenresId.Any())
            {
                books = books.Where(book => book.BookGenres.Any(bookGenre => paginatedBooksModel.GenresId.Contains(bookGenre.GenreId)));
            }
            if (paginatedBooksModel.FromDate == null && paginatedBooksModel.ToDate == null)
            {
                books = books.Skip((paginatedBooksModel.PageNumber - 1) * paginatedBooksModel.PageSize).Take(paginatedBooksModel.PageSize);
            }
            return books;
        }

    }
}
