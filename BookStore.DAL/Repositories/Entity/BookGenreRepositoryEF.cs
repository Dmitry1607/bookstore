﻿using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Entity
{
    public class BookGenreRepositoryEF : BaseRepositoryEF<BookGenre>, IBookGenreRepository
    {
        private readonly StoreContext _db;
        public BookGenreRepositoryEF(StoreContext context) : base(context)
        {
            _db = context;
        }
        public async Task<IEnumerable<BookGenre>> GetByBookId(string id)
        {
            return _db.BookGenres.Where(bookGenre => bookGenre.BookId == id).Include(bookGenre => bookGenre.Genre);
        }
        public async Task<IEnumerable<BookGenre>> GetIncludedlements()
        {
            return _db.BookGenres.Include(bookGenre => bookGenre.Book).Include(bookGenre => bookGenre.Genre);
        }
        public async Task AddRange(List<BookGenre> bookGenreList)
        {
            await _db.BookGenres.AddRangeAsync(bookGenreList);
            await _db.SaveChangesAsync();
        }
        public async Task RemoveRange(List<BookGenre> bookGenreList)
        {
            _db.BookGenres.RemoveRange(bookGenreList);
            await _db.SaveChangesAsync();
        }

        public async Task DeleteByBookId(string id)
        {
            _db.BookGenres.RemoveRange(_db.BookGenres.Where(bookGenre => bookGenre.Id == id));
            await _db.SaveChangesAsync();
        }
    }
}
