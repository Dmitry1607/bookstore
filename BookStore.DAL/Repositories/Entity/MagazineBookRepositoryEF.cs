﻿using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Entity
{
    public class MagazineBookRepositoryEF : BaseRepositoryEF<MagazineBook>, IMagazineBookRepository
    {
        private readonly StoreContext _db;
        public MagazineBookRepositoryEF(StoreContext context) : base(context)
        {
            _db = context;
        }
        public async Task<IEnumerable<MagazineBook>> GetIncludedElements()
        {
            return _db.MagazineBooks.Include(magazineBook => magazineBook.Magazine).Include(magazineBook => magazineBook.Book);
        }
        public async Task DeleteById(string id)
        {
            _db.MagazineBooks.RemoveRange(_db.MagazineBooks.Where(magazineBook => magazineBook.Id == id));
            await _db.SaveChangesAsync();
        }
        public async Task AddRange(List<MagazineBook> magazineBookList)
        {
            await _db.MagazineBooks.AddRangeAsync(magazineBookList);
            await _db.SaveChangesAsync();
        }
        public async Task RemoveRange(List<MagazineBook> magazineBookList)
        {
            _db.MagazineBooks.RemoveRange(magazineBookList);
            await _db.SaveChangesAsync();
        }
        public async Task<IEnumerable<MagazineBook>> GetByMagazineId(string id)
        {
            return _db.MagazineBooks.Where(magazineBook => magazineBook.MagazineId == id).Include(magazineBook => magazineBook.Book);
        }
    }
}
