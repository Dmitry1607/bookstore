﻿using System.Threading.Tasks;
using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace BookStore.DAL.Repositories.Entity
{
    public class AuthorRepositoryEF : BaseRepositoryEF<Author>, IAuthorRepository
    {
        private readonly StoreContext _db;
        public AuthorRepositoryEF(StoreContext context) : base(context)
        {
            _db = context;
        }

        public async Task<Author> GetAuthorByName(string name)
        {
            return await _db.Authors.Where(author => author.Name == name).FirstOrDefaultAsync();
        }
    }
}
