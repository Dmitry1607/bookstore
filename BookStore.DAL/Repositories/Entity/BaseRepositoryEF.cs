﻿using BookStore.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Entity
{
    public class BaseRepositoryEF<T> : IBaseRepository<T> where T : class
    {
        private readonly DbContext _context;
        private readonly DbSet<T> _dbSet;
        public BaseRepositoryEF(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }
        public async Task Create(T el)
        {
            await _dbSet.AddAsync(el);
            await _context.SaveChangesAsync();
        }
        public async Task Delete(string id)
        {
            T item = await _dbSet.FindAsync(id);
            if (item != null)
            {
                _dbSet.Attach(item);
                _dbSet.Remove(item);
            }
            await _context.SaveChangesAsync();
        }
        public async Task<T> Get(string id)
        {
            return await _dbSet.FindAsync(id);
        }
        public virtual async Task<IEnumerable<T>> Get()
        {
            return _dbSet;
        }
        public async Task Update(T el)
        {
            _context.Entry(el).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
