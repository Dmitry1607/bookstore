﻿using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Entity
{
    public class MagazineOrderRepositoryEF : BaseRepositoryEF<MagazineOrder>, IMagazineOrderRepository
    {
        private readonly StoreContext _db;
        public MagazineOrderRepositoryEF(StoreContext context) : base(context)
        {
            _db = context;
        }
        public async Task<IEnumerable<MagazineOrder>> GetIncludedElements()
        {
            return _db.MagazineOrders.Include(magazineOrder => magazineOrder.Magazine).Include(magazineOrder => magazineOrder.Order);
        }
        public async Task AddRange(IEnumerable<MagazineOrder> magazineOrders)
        {
            await _db.MagazineOrders.AddRangeAsync(magazineOrders);
            await _db.SaveChangesAsync();
        }
        public async Task<IEnumerable<string>> GetMagazineOrdersByOrderId(string id)
        {
            return _db.MagazineOrders.Where(magazineOrder => magazineOrder.OrderId == id).Select(magazineOrder => magazineOrder.MagazineId);
        }
    }
}
