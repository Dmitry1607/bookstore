﻿using BookStore.DAL.Interfaces;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;


namespace BookStore.DAL.Repositories.Dapper
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        private readonly string _tablename;
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public BaseRepository(string tablename, IConfiguration connection)
        {
            _tablename = tablename;
            _configuration = connection;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public async Task<IEnumerable<T>> Get()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var elements = (await connection.QueryAsync<T>("select * from " + _tablename));
                return elements;
            }
        }
        public async Task<T> Get(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                T element = (await connection.QueryAsync<T>("select * from " + _tablename + " where Id=@id", new { id })).FirstOrDefault();
                return element;
            }
        }
        public async Task Delete(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sqlQuery = "delete from " + _tablename + " where Id=@id";
                await connection.ExecuteAsync(sqlQuery, new { id });
            }
        }
        public async Task Create(T el)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.InsertAsync(el);
            }
        }
        public async Task Update(T el)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.UpdateAsync(el);
            }
        }
    }
}
