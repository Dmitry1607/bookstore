﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using BookStore.DAL.Models;

namespace BookStore.DAL.Repositories.Dapper
{
    public class BookRepository : BaseRepository<Book>, IBookRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public BookRepository(IConfiguration connection) : base("Books", connection)
        {
            _configuration = connection;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<int> GetMaxPriceAsync()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var sql = @"select max(Books.Price) from Books";
                var result = await connection.QueryFirstAsync<int>(sql);
                return result;
            }
        }
        public async Task<IEnumerable<Book>> Get(List<string> ids)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var sql = @"select distinct * from Books where Books.Id in @ids";
                var result = (await connection.QueryAsync<Book>(sql, new { ids })).ToList();
                return result;
            }
        }
        public async Task<IEnumerable<Book>> GetPaginatedBooks(PaginatedBooksModel paginatedBooksModel)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string condition = @"select distinct Books.Id, Books.CreationDate, Books.Title, Books.Price, Books.PublishDate, Books.ImageUrl, Books.Description from Books 
                            left join AuthorBooks on AuthorBooks.BookId=Books.Id left join BookGenres on BookGenres.BookId = Books.Id 
                            where Books.Price <= @maxPrice and Books.Price >= @minPrice ";
                string pagination = "order by Books.Id offset @skippedBooks rows fetch next @pageSize rows only ";
                var conditionList = new List<string>();
                if (paginatedBooksModel.AuthorsId.Any())
                {
                    conditionList.Add("AuthorBooks.AuthorId in @authorsId ");
                }
                if (paginatedBooksModel.GenresId.Any())
                {
                    conditionList.Add("BookGenres.GenreId in @genresId ");
                }
                if (conditionList.Any())
                {
                    condition = condition + "and " + string.Join("and ", conditionList);
                }
                if (paginatedBooksModel.FromDate.HasValue && paginatedBooksModel.ToDate.HasValue)
                {
                    condition += pagination;
                }
                var result = (await connection.QueryAsync<Book>(condition, new
                {
                    paginatedBooksModel.MaxPrice,
                    paginatedBooksModel.MinPrice,
                    paginatedBooksModel.AuthorsId,
                    paginatedBooksModel.GenresId,
                    skippedBooks = (paginatedBooksModel.PageNumber - 1) * paginatedBooksModel.PageSize,
                    paginatedBooksModel.PageSize
                }));
                return result;
            }
        }
        public async Task<int> GetPaginatedBooksCount(PaginatedBooksModel paginatedBooksModel)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string condition = @"select count(distinct Books.Id) from Books 
                            left join AuthorBooks on AuthorBooks.BookId=Books.Id left join BookGenres on BookGenres.BookId = Books.Id 
                            where Books.Price <= @maxPrice and Books.Price >= @minPrice ";
                var conditionList = new List<string>();
                if (paginatedBooksModel.AuthorsId.Any())
                {
                    conditionList.Add("AuthorBooks.AuthorId in @authorsId ");
                }
                if (paginatedBooksModel.GenresId.Any())
                {
                    conditionList.Add("BookGenres.GenreId in @genresId ");
                }
                if (conditionList.Any())
                {
                    condition = condition + "and " + string.Join("and ", conditionList);
                }
                string sql = condition;
                var result = await connection.QueryFirstAsync<int>(sql, new
                {
                    paginatedBooksModel.AuthorsId,
                    paginatedBooksModel.GenresId,
                    paginatedBooksModel.MaxPrice,
                    paginatedBooksModel.MinPrice
                });
                return result;
            }
        }
    }
}
