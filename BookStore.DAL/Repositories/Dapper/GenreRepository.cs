﻿using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using Microsoft.Extensions.Configuration;

namespace BookStore.DAL.Repositories.Dapper
{
    public class GenreRepository : BaseRepository<Genre>, IGenreRepository
    {
        private readonly IConfiguration _configuration;
        public GenreRepository(IConfiguration connection) : base("Genres", connection)
        {
            _configuration = connection;
        }
    }
}
