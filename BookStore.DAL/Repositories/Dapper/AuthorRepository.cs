﻿using System.Threading.Tasks;
using Dapper;
using BookStore.DAL.Interfaces;
using BookStore.DAL.Entities;
using System.Linq;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace BookStore.DAL.Repositories.Dapper
{
    public class AuthorRepository : BaseRepository<Author>, IAuthorRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public AuthorRepository(IConfiguration connection) : base("Authors", connection)
        {
            _configuration = connection;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<Author> GetAuthorByName(string name)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = @"select * from Authors where Authors.Name=@name";
                var result = (await connection.QueryAsync<Author>(sql, new { name })).FirstOrDefault();
                return result;
            }
        }
    }
}
