﻿using BookStore.DAL.Entities;
using BookStore.DAL.Interfaces;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BookStore.DAL.Repositories.Dapper
{
    public class BookGenreRepository : BaseRepository<BookGenre>, IBookGenreRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public BookGenreRepository(IConfiguration connection) : base("BookGenres", connection)
        {
            _configuration = connection;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task AddRange(List<BookGenre> bookGenreList)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.InsertAsync(bookGenreList);
            }
        }
        public async Task<IEnumerable<BookGenre>> GetByBookId(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * from BookGenres as bg join Genres as g on bg.GenreId=g.Id join Books as b on bg.BookId=b.Id where bg.BookId=@id";
                var result = await connection.QueryAsync<BookGenre, Genre, Book, BookGenre>(sql, (bookGenre, genre, book) =>
                {
                    bookGenre.Genre = genre;
                    bookGenre.Book = book;
                    return bookGenre;
                }, new { id }, splitOn: "Id, Id");
                return result;
            }
        }
        public async Task RemoveRange(List<BookGenre> bookGenreList)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.DeleteAsync(bookGenreList);
            }
        }
        public async Task DeleteByBookId(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = @"DELETE from BookGenres where BookGenres.BookId=@id";
                await connection.ExecuteAsync(sql, new { id });
            }
        }
        public async Task<IEnumerable<BookGenre>> GetIncludedlements()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * from BookGenres as bg join Genres as g on bg.GenreId=g.Id join Books as b on bg.BookId=b.Id";
                var result = await connection.QueryAsync<BookGenre, Genre, Book, BookGenre>(sql, (bookGenre, genre, book) =>
                {
                    bookGenre.Genre = genre;
                    bookGenre.Book = book;
                    return bookGenre;
                }, splitOn: "Id, Id");
                return result;
            }
        }
    }
}
