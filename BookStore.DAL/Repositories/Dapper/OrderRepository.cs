﻿using System.Threading.Tasks;
using Dapper;
using BookStore.DAL.Interfaces;
using BookStore.DAL.Entities;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace BookStore.DAL.Repositories.Dapper
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public OrderRepository(IConfiguration connection) : base("Orders", connection)
        {
            _configuration = connection;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<IEnumerable<Order>> GetByUserId(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var sql = @"select * from Orders where Orders.UserId=@id";
                var result = (await connection.QueryAsync<Order>(sql, new
                { id }));
                return result;
            }
        }
    }
}
