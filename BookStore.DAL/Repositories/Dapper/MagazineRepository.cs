﻿using System.Threading.Tasks;
using Dapper;
using BookStore.DAL.Interfaces;
using BookStore.DAL.Entities;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace BookStore.DAL.Repositories.Dapper
{
    public class MagazineRepository : BaseRepository<Magazine>, IMagazineRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public MagazineRepository(IConfiguration connection) : base("Magazines", connection)
        {
            _configuration = connection;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<IEnumerable<Magazine>> GetElements(List<string> ids)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var sql = @"select distinct * from Magazines where Magazines.Id in @ids";
                var result = (await connection.QueryAsync<Magazine>(sql, new { ids })).ToList();
                return result;
            }
        }
    }
}
