﻿using System.Threading.Tasks;
using Dapper;
using BookStore.DAL.Interfaces;
using BookStore.DAL.Entities;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace BookStore.DAL.Repositories.Dapper
{
    public class BookOrderRepository : BaseRepository<BookOrder>, IBookOrderRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public BookOrderRepository(IConfiguration connection) : base("BookOrders", connection)
        {
            _configuration = connection;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task AddRange(List<BookOrder> bookOrders)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.InsertAsync(bookOrders);
            }
        }

        public async Task<IEnumerable<string>> GetBookOrdersByOrderId(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = @"select BookOrders.BookId from BookOrders where BookOrders.OrderId=@id";
                var result = (await connection.QueryAsync<string>(sql, new
                { id }));
                return result;
            }
        }

        public async Task<IEnumerable<BookOrder>> GetIncludedElements()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * from BookOrders as bo join Orders as o on bo.OrderId=o.Id join Books as b on bo.BookId=b.Id";
                var result = await connection.QueryAsync<BookOrder, Order, Book, BookOrder>(sql, (bookOrder, order, book) =>
                {
                    bookOrder.Order = order;
                    bookOrder.Book = book;
                    return bookOrder;
                }, splitOn: "Id, Id");
                return result;
            }
        }
    }
}
