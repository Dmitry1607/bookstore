﻿using System.Threading.Tasks;
using Dapper;
using BookStore.DAL.Interfaces;
using BookStore.DAL.Entities;
using System.Collections.Generic;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace BookStore.DAL.Repositories.Dapper
{
    public class MagazineOrderRepository : BaseRepository<MagazineOrder>, IMagazineOrderRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public MagazineOrderRepository(IConfiguration connection) : base("MagazineOrders", connection)
        {
            _configuration = connection;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task AddRange(IEnumerable<MagazineOrder> magazineOrders)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.InsertAsync(magazineOrders);
            }
        }

        public async Task<IEnumerable<string>> GetMagazineOrdersByOrderId(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var sql = @"select MagazineOrders.MagazineId from MagazineOrders where MagazineOrders.OrderId=@id";
                var result = (await connection.QueryAsync<string>(sql, new
                { id }));
                return result;
            }
        }

        public async Task<IEnumerable<MagazineOrder>> GetIncludedElements()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var sql = @"SELECT * from MagazineOrders as mo join Orders as o on mo.OrderId=o.Id join Magazines as m on mo.MagazineId=m.Id";
                var result = await connection.QueryAsync<MagazineOrder, Order, Magazine, MagazineOrder>(sql, (magazineOrder, order, magazine) =>
                {
                    magazineOrder.Order = order;
                    magazineOrder.Magazine = magazine;
                    return magazineOrder;
                }, splitOn: "Id, Id");
                return result;
            }
        }
    }
}
