﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using BookStore.DAL.Interfaces;
using BookStore.DAL.Entities;
using Dapper.Contrib.Extensions;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace BookStore.DAL.Repositories.Dapper
{
    public class AuthorBookRepository : BaseRepository<AuthorBook>, IAuthorBookRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;

        public AuthorBookRepository(IConfiguration connection) : base("AuthorBooks", connection)
        {
            _configuration = connection;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task AddRange(List<AuthorBook> authorBookList)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.InsertAsync(authorBookList);
            }
        }
        public async Task<IEnumerable<AuthorBook>> GetByBookId(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = $@"SELECT * from AuthorBooks as ab join Authors as a on ab.AuthorId=a.Id where ab.BookId=@id";
                var result = await connection.QueryAsync<AuthorBook, Author, AuthorBook>(sql, (authorBook, author) =>
                {
                    authorBook.Author = author;
                    return authorBook;
                }, new { id }, splitOn: "Id");
                return result;
            }
        }
        public async Task DeleteByBookId(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = @"DELETE from AuthorBooks where AuthorBooks.BookId=@id";
                await connection.ExecuteAsync(sql, new { id });
            }
        }
        public async Task RemoveRange(List<AuthorBook> authorBookList)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.DeleteAsync(authorBookList);
            }
        }

        public async Task<IEnumerable<AuthorBook>> GetIncludedElements()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                string sql = @"SELECT * from AuthorBooks as ab join Authors as a on ab.AuthorId=a.Id join Books as b on ab.BookId=b.Id ";
                var result = await connection.QueryAsync<AuthorBook, Author, Book, AuthorBook>(sql, (authorBook, author, book) =>
                {
                    authorBook.Author = author;
                    authorBook.Book = book;
                    return authorBook;
                }, splitOn: "Id, Id");
                return result;
            }
        }
    }
}