﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using BookStore.DAL.Interfaces;
using BookStore.DAL.Entities;
using System.Linq;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using BookStore.DAL.Models;

namespace BookStore.DAL.Repositories.Dapper
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public UserRepository(IConfiguration connection) : base("AspNetUsers", connection)
        {
            _configuration = connection;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }
        public async Task<IEnumerable<AspNetUserRole>> GetUsersWithRoles()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var lookup = new Dictionary<string, AspNetUserRole>();
                string sql = @"SELECT * from AspNetUserRoles as anur
                          join AspNetRoles as anr ON anur.RoleId = anr.Id";
                var result = await connection.QueryAsync<AspNetUserRole, AspNetRole, AspNetUserRole>(sql,
                    (ur, r) =>
                    {
                        AspNetUserRole userRole;
                        if (!lookup.TryGetValue(ur.UserId, out userRole))
                        {
                            lookup.Add(ur.UserId, userRole = ur);
                        }
                        if (userRole.Role == null)
                        {
                            userRole.Role = new List<AspNetRole>();
                        }
                        userRole.Role.Add(r);
                        return userRole;
                    });
                return result;
            }
        }
        public async Task<IEnumerable<User>> GetUsersIncluded()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var lookup = new Dictionary<string, User>();
                (await connection.QueryAsync<User, Order, User>(@"select u.*, o.* 
                                                   from AspNetUsers u 
                                                   inner join Orders o on o.UserId=u.Id", (u, o) =>
            {
                User user;
                if (!lookup.TryGetValue(u.Id, out user))
                {
                    lookup.Add(u.Id, user = u);
                }
                if (user.Orders == null)
                {
                    user.Orders = new List<Order>();
                }
                user.Orders.Add(o);
                return user;
            })).AsQueryable();
                var resultList = lookup.Values;
                return resultList;
            }
        }
    }
}
