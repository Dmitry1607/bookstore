﻿using System.Threading.Tasks;
using Dapper;
using BookStore.DAL.Interfaces;
using BookStore.DAL.Entities;
using System.Collections.Generic;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace BookStore.DAL.Repositories.Dapper
{
    public class MagazineBookRepository : BaseRepository<MagazineBook>, IMagazineBookRepository
    {
        private readonly IConfiguration _configuration;
        private readonly string _connectionString;
        public MagazineBookRepository(IConfiguration connection) : base("MagazineBooks", connection)
        {
            _configuration = connection;
            _connectionString = _configuration.GetConnectionString("DefaultConnection");
        }

        public async Task AddRange(List<MagazineBook> magazineBookList)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.InsertAsync(magazineBookList);
            }
        }

        public async Task<IEnumerable<MagazineBook>> GetByMagazineId(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var sql = $@"SELECT * from MagazineBooks as mb join Books as b on mb.BookId=b.Id join Magazines as m on mb.MagazineId=m.Id where mb.MagazineId=@id";
                var result = await connection.QueryAsync<MagazineBook, Book, Magazine, MagazineBook>(sql, (magazineBook, book, magazine) =>
                {
                    magazineBook.Book = book;
                    magazineBook.Magazine = magazine;
                    return magazineBook;
                }, new { id }, splitOn: "Id, Id");
                return result;
            }
        }
        public async Task DeleteById(string id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var sql = @"delete from MagazineBooks where MagazineBooks.MagazineId=@id";
                await connection.ExecuteAsync(sql, new { id });
            }
        }
        public async Task<IEnumerable<MagazineBook>> GetIncludedElements()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var sql = @"SELECT * from MagazineBooks as mb join Books as b on mb.BookId=b.Id join Magazines as m on mb.MagazineId=m.Id";
                var result = await connection.QueryAsync<MagazineBook, Book, Magazine, MagazineBook>(sql, (magazineBook, book, magazine) =>
                {
                    magazineBook.Book = book;
                    magazineBook.Magazine = magazine;
                    return magazineBook;
                }, splitOn: "Id, Id");
                return result;
            }
        }

        public async Task RemoveRange(List<MagazineBook> magazineBookList)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.DeleteAsync(magazineBookList);
            }
        }
    }
}
