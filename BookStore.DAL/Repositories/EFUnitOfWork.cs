﻿using System;
using BookStore.DAL.Interfaces;
using BookStore.DAL.Repositories.Entity;
using Microsoft.EntityFrameworkCore;

namespace BookStore.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly StoreContext _db;
        private AuthorBookRepositoryEF _authorBookRepository;
        private AuthorRepositoryEF _authorRepository;
        private BookOrderRepositoryEF _bookOrderRepository;
        private BookRepositoryEF _bookRepository;
        private MagazineBookRepositoryEF _magazineBookRepository;
        private MagazineOrderRepositoryEF _magazineOrderRepository;
        private MagazineRepositoryEF _magazineRepository;
        private OrderRepositoryEF _orderRepository;
        private UserRepositoryEF _userRepository;
        private GenreRepositoryEF _genreRepository;
        private BookGenreRepositoryEF _bookGenreRepository;
        public EFUnitOfWork(DbContextOptions<StoreContext> options)
        {
            _db = new StoreContext(options);
        }
        public IAuthorBookRepository AuthorBooks
        {
            get
            {
                if (_authorBookRepository == null)
                    _authorBookRepository = new AuthorBookRepositoryEF(_db);
                return _authorBookRepository;
            }
        }
        public IAuthorRepository Authors
        {
            get
            {
                if (_authorRepository == null)
                    _authorRepository = new AuthorRepositoryEF(_db);
                return _authorRepository;
            }
        }
        public IBookOrderRepository BookOrders
        {
            get
            {
                if (_bookOrderRepository == null)
                    _bookOrderRepository = new BookOrderRepositoryEF(_db);
                return _bookOrderRepository;
            }
        }
        public IBookRepository Books
        {
            get
            {
                if (_bookRepository == null)
                    _bookRepository = new BookRepositoryEF(_db);
                return _bookRepository;
            }
        }
        public IMagazineBookRepository MagazineBooks
        {
            get
            {
                if (_magazineBookRepository == null)
                    _magazineBookRepository = new MagazineBookRepositoryEF(_db);
                return _magazineBookRepository;
            }
        }
        public IMagazineOrderRepository MagazineOrders
        {
            get
            {
                if (_magazineOrderRepository == null)
                    _magazineOrderRepository = new MagazineOrderRepositoryEF(_db);
                return _magazineOrderRepository;
            }
        }
        public IMagazineRepository Magazines
        {
            get
            {
                if (_magazineRepository == null)
                    _magazineRepository = new MagazineRepositoryEF(_db);
                return _magazineRepository;
            }
        }
        public IOrderRepository Orders
        {
            get
            {
                if (_orderRepository == null)
                    _orderRepository = new OrderRepositoryEF(_db);
                return _orderRepository;
            }
        }
        public IUserRepository Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepositoryEF(_db);
                return _userRepository;
            }
        }
        public IGenreRepository Genres
        {
            get
            {
                if (_genreRepository == null)
                    _genreRepository = new GenreRepositoryEF(_db);
                return _genreRepository;
            }
        }
        public IBookGenreRepository BookGenres
        {
            get
            {
                if (_bookGenreRepository == null)
                    _bookGenreRepository = new BookGenreRepositoryEF(_db);
                return _bookGenreRepository;
            }
        }
        public void Save()
        {
            _db.SaveChanges();
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
